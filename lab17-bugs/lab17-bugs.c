#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/dma-mapping.h>
#include <linux/dma-direct.h>
#include <linux/timer.h>
#include <linux/delay.h>

#define MY_DEV_NAME "my-device"

#define SIZE 128

static unsigned int alloc_method = 0;
module_param(alloc_method, uint, 0000);
MODULE_PARM_DESC(alloc_method, "0=kmalloc, 1=attrs, 2=get-free-pages");


enum alloc_method_e {
	ALLOC_KMALLOC,
	ALLOC_ATTRS,
	ALLOC_GET_FREE_PAGES,
	ALLOC_NONCONT,
	NUM_ALLOC_METHODS
};

struct my_driver {
	struct platform_device* pdev;
	 char *buf;
	dma_addr_t dma_addr;
	struct sg_table *sg;
};

static DEFINE_MUTEX(my_mutex);
static struct my_driver* mdriver = NULL;


int my_mmap(struct file *file, struct vm_area_struct *vma)
{
	if( remap_pfn_range (vma, vma->vm_start, vma->vm_pgoff,
		vma->vm_end - vma->vm_start, vma->vm_page_prot ))
		return -EAGAIN;
	return 0;
}
/* ===================== get - free - pages ============ */
/* After unmap. */
static void vm_close(struct vm_area_struct *vma)
{
	pr_info("vm_close\n");
}

/* First page access. */
static vm_fault_t vm_fault(struct vm_fault *vmf)
{
	struct page *page;
	char *buf;

	pr_err("vm_fault\n");
	buf = (char*)vmf->vma->vm_private_data;
	if (buf) {
		page = virt_to_page(buf);
		get_page(page);
		vmf->page = page;
	}
	return 0;
}

/* After mmap. TODO vs mmap, when can this happen at a different time than mmap? */
static void vm_open(struct vm_area_struct *vma)
{
	pr_info("vm_open\n");
}

static struct vm_operations_struct vm_ops =
{
	.close = vm_close,
	.fault = vm_fault,
	.open = vm_open,
};

static int get_free_pages_mmap(struct file *file, struct vm_area_struct *vma)
{
	pr_err("%s:\n", __func__);
	vma->vm_ops = &vm_ops;
	vma->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP;
	vma->vm_private_data = mdriver->buf;
	pr_err("mapped dma addr 0x%08lx at 0x%08lx, size %d\n",
	       (unsigned long)mdriver->dma_addr, vma->vm_start, SIZE);
	pr_err("kernel cpu addr is 0x%08lx\n", (unsigned long)mdriver->buf);
	pr_err("phys addr is 0x%08llx\n", virt_to_phys(mdriver->buf));
	vm_open(vma);
	return 0;
}

/* ===================== end =============== */

static int attrs_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct device *dev = &mdriver->pdev->dev;
	int ret;

	pr_err("lab7 dma driver - mmap\n");

	ret = dma_mmap_attrs(dev, vma, mdriver->buf,
                mdriver->dma_addr, SIZE, 0);
	if (ret) {
		pr_err("error\n");
		return ret;
	}
	pr_err("mapped dma addr 0x%08lx at 0x%08lx, size %d\n",
	       (unsigned long)mdriver->dma_addr, vma->vm_start, SIZE);
	pr_err("kernel cpu addr is 0x%08lx\n", (unsigned long)mdriver->buf);
	pr_err("phys addr is 0x%08llx\n", virt_to_phys(mdriver->buf));
	return 0;
}

static int noncont_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct device *dev = &mdriver->pdev->dev;
	return dma_mmap_noncontiguous(dev, vma, SIZE, mdriver->sg);
}

static int switch_mmap(struct file *file, struct vm_area_struct *vma)
{
	switch(alloc_method) {
		case ALLOC_ATTRS:
			return attrs_mmap(file, vma);
			break;
		case ALLOC_NONCONT:
			return noncont_mmap(file, vma);
			break;
		case ALLOC_KMALLOC:
		case ALLOC_GET_FREE_PAGES:
			return get_free_pages_mmap(file, vma);
			break;
		default:
			pr_err("bad_alloc_method");
			return -EINVAL;
			break;
	}
	return 0;
}

static int mycdrv_open(struct inode *inode, struct file *file)
{
	/* Unable to handle kernel paging request at virtual address aeebbaaeebbaaf33 ? */
	//int *x = vmalloc(4096);
	//int *x = kmalloc(4096, GFP_KERNEL);
	//int *x = ERR_PTR(-EINVAL);
	//int *x = 0; /* BUG: kernel NULL pointer dereference, address: 0000000000000000 */
	//int *x = 1; /* BUG: kernel NULL pointer dereference, address: 0000000000000001 */
	int *x = 1000000; /* BUG: unable to handle page fault for address: 00000000000f4240 */
	//if (!x)
	//	return -ENOMEM;
	pr_err("OPEN device\n");
	while (true) {
		int a = *x;
		pr_err("%d %px\n", a, x);
		x += 4096;
		msleep(10);
	}
	mutex_lock(&my_mutex);
	/* can mdriver be NULL here ? */
	file->private_data = mdriver ? mdriver->pdev : NULL;
	mutex_unlock(&my_mutex);
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	mdriver->buf[5] = 0;
	pr_info("CLOSING device '%s' good\n", mdriver->buf);

	file->private_data = NULL;
	return 0;
}


static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .open = mycdrv_open,
        .release = mycdrv_release,
	//.mmap = mycdrv_mmap
	.mmap = switch_mmap
};

static unsigned int count = 1;

//mknod /dev/lab3 c 455 0
static int my_major = 455, my_minor = 0;
static struct cdev my_cdev;

static void free_driver(void)
{
	mutex_lock(&my_mutex);
	if (mdriver) {
		kfree(mdriver);
		mdriver = NULL;
	}
	mutex_unlock(&my_mutex);
}


static int get_free_pages_dma_buf(struct my_driver *mdriver)
{
	struct device *dev = &mdriver->pdev->dev;
	char *buf;

	mdriver->buf =  (unsigned char *) __get_free_page(GFP_KERNEL);
	if(!mdriver->buf)
		return -ENOMEM;
	buf = mdriver->buf;
	pr_err("== dma_map_single ==\n");
	pr_err("buf 0x%08lx\n", (unsigned long)buf);
	pr_err("virt_to_phys(buf): 0x%08llx\n", virt_to_phys(buf));
	pr_err("page_to_phys(virt_to_page(buf)): 0x%08llx\n",
		page_to_phys(virt_to_page(buf)));
	mdriver->dma_addr = dma_map_single(dev, mdriver->buf, SIZE, 0);
	if (dma_mapping_error(dev, mdriver->dma_addr)) {
		kfree(mdriver->buf);
		return -ENOMEM;
	}
	return 0;
}

static int noncont_dma_buf(struct my_driver *mdriver)
{
	struct device *dev = &mdriver->pdev->dev;
	mdriver->sg = dma_alloc_noncontiguous(dev, SIZE, DMA_BIDIRECTIONAL,
					     GFP_KERNEL, 0);

	if (!mdriver->sg)
		return -ENOMEM;
	return 0;
}


static int kzalloc_dma_buf(struct my_driver *mdriver)
{
	struct device *dev = &mdriver->pdev->dev;
	char *buf;

	mdriver->buf = kzalloc(SIZE, GFP_KERNEL);
	if(!mdriver->buf)
		return -ENOMEM;
	buf = mdriver->buf;
	pr_err("== dma_map_single ==\n");
	pr_err("buf 0x%08lx\n", (unsigned long)buf);
	pr_err("virt_to_phys(buf): 0x%08llx\n", virt_to_phys(buf));
	pr_err("page_to_phys(virt_to_page(buf)): 0x%08llx\n",
		page_to_phys(virt_to_page(buf)));
	mdriver->dma_addr = dma_map_single(dev, mdriver->buf, SIZE, 0);
	if (dma_mapping_error(dev, mdriver->dma_addr)) {
		kfree(mdriver->buf);
		return -ENOMEM;
	}
	return 0;
}


static int alloc_attrs_dma_buf(struct my_driver *mdriver)
{
	struct device *dev = &mdriver->pdev->dev;
	char *buf;

	dev_err(dev, "%s: == dma_alloc_attrs ==\n", __func__);
	mdriver->buf = dma_alloc_attrs(dev, SIZE, &mdriver->dma_addr,
	                               GFP_KERNEL, 0);
	buf = mdriver->buf;
	pr_err("buf 0x%08lx\n", (unsigned long)buf);
	pr_err("is_vmalloc_addr(buf) %d\n", is_vmalloc_addr(buf));
	pr_err("virt_addr_valid(buf) %d\n", virt_addr_valid(buf));
	pr_err("virt_to_phys(buf): 0x%08llx\n", virt_to_phys(buf));
	pr_err("page_to_phys(virt_to_page(buf)): 0x%08llx\n",
		page_to_phys(virt_to_page(buf)));
	pr_err("page_to_phys(vmalloc_to_page(buf)): 0x%08llx\n\n",
		page_to_phys(vmalloc_to_page(buf)));

	pr_err("dma_handle 0x%08llx\n", mdriver->dma_addr);
	pr_err("phys_to_dma(dev, virt_to_phys(buf)): 0x%08llx\n",
		phys_to_dma(dev, virt_to_phys(buf)));
	pr_err("phys_to_dma(dev, page_to_phys(virt_to_page(buf))): 0x%08llx\n",
		phys_to_dma(dev, page_to_phys(virt_to_page(buf))));
	pr_err("virt_to_page(buf): 0x%08lx\n",
		(unsigned long)virt_to_page(buf));
	if(!mdriver->buf)
		return -ENOMEM;
	return 0;
}


static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	if(alloc_method >= NUM_ALLOC_METHODS) {
			pr_err("bad alloc method %d\n", alloc_method);
			return -EINVAL;
	}

	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_lock(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);

	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;
	mutex_unlock(&my_mutex);

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}

	platform_set_drvdata(pdev, mdriver);
	switch(alloc_method) {
		case ALLOC_KMALLOC:
			kzalloc_dma_buf(mdriver);
			break;
		case ALLOC_ATTRS:
			alloc_attrs_dma_buf(mdriver);
			break;
		case ALLOC_NONCONT:
			noncont_dma_buf(mdriver);
			break;
		case ALLOC_GET_FREE_PAGES:
			get_free_pages_dma_buf(mdriver);
			break;
		default:
			pr_err("bad alloc method %d\n", alloc_method);
			return -EINVAL;
	}
	dev_info(&pdev->dev, "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static struct platform_device my_device = {
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;
	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
