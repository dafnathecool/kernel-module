I wanted for a while to move to mutt but was every time too lazy.


* First, you have to configure your .muttrc file.
There are some tutorials out there on how to do that.
I followed more or less this: https://gideonwolfe.com/posts/workflow/neomutt/intro/

* Then you just open mutt with `mutt`.
* when entering mutt you probably see the list of your mails, this is called
in mutt's jargon the `index` you can navigate it up and down with the arrows
or the `k`, `j` keys like in vim. `Enter` on a mail line will show you
the content of the mail in a separate screen called in mutt `pager`
to get out of the `pager` press `q`.
Both the `index` and the `pager` and any other mutt "part" is called
a `menu`.
There are some concepts in mutt that are a bit confusing. So here are some notations:
command - on the 'index' screen, you run a command by typing ":" and then the name of command. You can
use <tab> for autocomplete of the command.
function - this is something you tell mutt to do, like sort the messages etc. On the mutt configuration,
you can bind a function to a key. (^A stands for ctrl+A). To see the list of functions binding press "?"
in the 'index' screen. If a function is not binded, you can run it with the command ":exec <function>"
macro - with a macro you can bind a list of functions and commands to a key.
hook - bind a shell command/script to a key.


Long story short - I moved from mutt to neomutt, since I didn't like
the fact that `sort_aux` is used to sort both the threads themself
and the messages inside a thread.
Then I found out that Ubunut's neomutt package is extremely old.
So I compiled my own with the help of Sebastian:
```
# Install Neomutt
#-------------------------------------
sudo apt install -y libidn11 libidn11-dev libncursesw5 libncursesw5-dev libxml2-utils docbook-xsl w3m libgpgme-dev libgnutls28-dev notmuch libtokyocabinet-dev libnotmuch-dev libnotmuch5 libpcre2-dev xsltproc
cd ~/Apps && wget https://github.com/neomutt/neomutt/archive/refs/tags/20211029.tar.gz && tar -xf 20211029.tar.gz && rm 20211029.tar.gz\
 && cd neomutt-20211029 && ./configure --gnutls --gpgme --disable-nls --notmuch --pcre2 --tokyocabinet && make
```
Then instead of installing it with `make install` I created an alias.
In addition better to uninstall neomutt and mutt if they are installed from the apt
Then when loading I see the message:

```
This certificate belongs to:
   ISRG Root X1
   Internet Security Research Group
   US

This certificate was issued by:
...
```
I found following discussion about it here:
https://gist.github.com/bnagy/8914f712f689cc01c267

I found out that ssl_usesystemcerts is not recognized.

# common scenario: apply a patchset from mail in a git repo:

* go to a folder and save a thread of patchset locally and `git am` that thread.

The simple why:
1. go to a folder with `c` and then `?` shows the list of folders. Go to the
folder where the thread resides.
2. search for the thread with `/`.
3. `Esc+t` to tag the thread
4. press `;` to start operation on the tagged patches then `C` to save the whole
thread to a local file. Depending on the mailbox format it will either save it
in an `.mbox` file or a maildir directory containing `cur`, `new` , `tmp`.
Note that you want `C` and not `s`, though `s` is for `save-message`
in mutt it actually means `move` so that the original message is deleted.

From docs:
Once you have tagged the desired messages, you can use the “tag-prefix” operator, which is the “;”(semicolon) key by default. When the “tag-prefix” operator is used, the next operation will be applied to all tagged messages if that operation can be used in that manner. If the $auto_tag variable is set, the next operation applies to the tagged messages automatically, without requiring the “tag-prefix”.

Once you have tagged the desired messages, you can use the “tag-prefix” operator, which is the “;”(semicolon) key by default. When the “tag-prefix” operator is used, the next operation will be applied to all tagged messages if that operation can be used in that manner. If the $auto_tag variable is set, the next operation applies to the tagged messages automatically, without requiring the “tag-prefix”.

5. Now I see some of the messages are identical (duplicates) let's see
how `git am` have to say. First I see I have old git version, new version
has to option `--empty=drop` to drop empty commits, this fixes problem
of trying to apply duplicates or the cover letter. In my case I had
to run `git am --skip` for every empty patch.
But it generally seems to work:)

Now with mutt there was a problem since patches inside the thread
were not ordered first to last so git tried to apply the last patch
in the set first causing obviously conflicts.

With neomutt, my sorting config is:
```
set use_threads = yes
set sort = reverse-date-received # thread with message coming las
set sort_aux = subject
set sort_re
```
note, this works with the manually compiled neomutt.
Currently I have neomutt from the 'apt' not manually compiled.
mutt is a bit of a fighting. Hard to get it right.
Another option is to delete all messages that are not a patch. This can be done wihth pressing
'D' when on the message. This will add the message to "marked to delete" list.
To actually delete, you should do 'sync-mailbox' which is binded to '<tab>'. or '$'
Then call ":exec sort-mailbox' to sort by subject. Then again toggle and save to another mailbox:
'Esc+t' , ';' , 'C'
Then I can use 'git am ~/Mail/my-mbox.mbox' to apply all patches.
Note that often in the cover letter it will be written that the patches is not top of another patchset
so you have to first apply the first patchset otherwise 'git am' will compline 'patchset not applied'

Another example: remove all replying messages in thread:
0. press ctrl+t and then '~T' to untag already tagge messages.
1. press 'T'
2. write '~s "Re:"' to tag messages with the 'Re:' in subject
3. press ';" and then 'D' to delete
4. press '$' to save chnages




# setting index format
What I would like:
message number "%4C", flags "%Z", full date "%D", from "%n" , subject %s,
j

```
set date_format = "%d.%m.%Y %H:%M"
set index_format = "%4C [%Z] %D %-30.30n %s"
```

# TODO
* change colors!!, I don't like this default bright blue/grey background
* set pager to be smaller so I have about half screen the page, and half the index

```
set pager_index_lines = 10
```

* don't show all those headers in the pager, show only the important (To/From/Subject) etc

```
# by default all headers are shown in the pager.
# this setting ignore by default all headers and then unignore the interesting once
# note: you can click 'h' to show all headers and 'h' again to undo
ignore *
unignore from date subject to cc bcc
unignore List-ID
unignore x-mailer
unignore x-spam-status
unignore x-spam-report
```

* set the side bar to show all folders all the time like in thunderbird and
the ability to navigate there

* experiment with macros:
1. 

# lei
Based on:
https://people.kernel.org/monsieuricon/lore-lei-part-1-getting-started

Installing lei:
(This will install lei in '~/bin' which then should be added to `$PATH`

```
sudo apt install libdbi-perl
sudo apt install liblinux-inotify2-perl
sudo apt install p5-IO-KQueue
sudo apt install libdbd-sqlite3-perl
sudo apt install libsearch-xapian-perl
sudo apt install curl

git clone https://public-inbox.org/public-inbox.git
# then follow the install instructions:
perl Makefile.PL
make
make test # we can see if test is skipped due to missing library and then install that library
make symlink-install prefix=$HOME
```


1. scenario:
 - A collague ask you to apply a patchset and test it. It gives you a link to the patchset, 
   For example: https://lore.kernel.org/linux-mediatek/20210825063323.3607738-1-eizan@chromium.org/

For this specific case you might want to use `b4` instaed of `lei`:
```
dafna:~/git/media_tree$ b4 am -3 -g 20210825063323.3607738-1-eizan@chromium.org
Looking up https://lore.kernel.org/r/20210825063323.3607738-1-eizan%40chromium.org
Grabbing thread from lore.kernel.org/all
Analyzing 21 messages in the thread
---
Writing ./v7_20210825_eizan_refactor_mtk_mdp_driver_into_core_components.mbx
  ✓ [PATCH v7 1/7] mtk-mdp: propagate errors from clock_on
  ✓ [PATCH v7 2/7] mtk-mdp: add driver to probe mdp components
  ✓ [PATCH v7 3/7] mtk-mdp: use pm_runtime in MDP component driver
  ✓ [PATCH v7 4/7] media: mtk-mdp: don't pm_run_time_get/put for master comp in clock_on
  ✓ [PATCH v7 5/7] mtk-mdp: make mdp driver to be loadable by platform_device_register*()
  ✓ [PATCH v7 6/7] soc: mediatek: mmsys: instantiate mdp virtual device from mmsys
  ✓ [PATCH v7 7/7] media: mtk-mdp: use mdp-rdma0 alias to point to MDP master
  ---
  ✓ Attestation-by: DKIM/chromium.org (From: eizan@chromium.org)
---
Total patches: 7
Preparing fake-am for v7: Refactor MTK MDP driver into core/components
  range: a27d0eb26168..7c87c643d76f
Prepared a fake commit range for 3-way merge (a27d0eb26168..7c87c643d76f)
---
Cover: ./v7_20210825_eizan_refactor_mtk_mdp_driver_into_core_components.cover
 Link: https://lore.kernel.org/r/20210825063323.3607738-1-eizan@chromium.org
 Base: not found (best guess: current tree)
       git am ./v7_20210825_eizan_refactor_mtk_mdp_driver_into_core_components.mbx

dafna:~/git/media_tree$ git am -3 ./v7_20210825_eizan_refactor_mtk_mdp_driver_into_core_components.mbx
```

2.  scenario:
- you are the maintainer of driver `drivers/mysubsys/mybaby/`, you want to see all patches sent to this driver.
  What you might want is whole mails mentionening `mybaby` so that it catches also patches sent to `Documentation`, `dt-binding` etc.
  Now, if you already have a local mailbox with all those mails, all you want is to update that mailbox with new mails.
  If you don't have such mailbox, you want to create it and intitiate it with those mails.

  Let's look at all patches mentioning `rkisp1`
  
```
mkdir ~/Mail/rkisp
lei q -I https://lore.kernel.org/all/ -o ~/Mail/rkisp1 --threads --dedupe=mid '(dfn:drivers/media/platform/rockchip/rkisp1 OR dfa:rkisp1 OR dfb:rkisp1 OR dfhh:rkisp1 OR dfctx:rkisp1 OR s:rkisp1) AND rt:5.year.ago..'
```
  See https://lore.kernel.org/all/_/text/help/ for the different fields. Short list:
few of them:
dfa=match diff removed lines
dfb=match diff added lines
dfhh=match diff hank context(usually a function name)
dfctx=match diff context lines
s=match subject


  This will also downloads all patchsets to the stabel produced by G K-H and the autoselceted from Sasha Levin. We don't want that.
  To remove exisitng search associated with a folder use 'lei forget-search' so we first do 'lei forget-search ~/Mail/rkisp1', and then
  Let's try again:
```
lei q -I https://lore.kernel.org/all/ -o ~/Mail/rkisp1 --threads --dedupe=mid '(dfn:drivers/media/platform/rockchip/rkisp1 OR dfa:rkisp1 OR dfb:rkisp1 OR dfhh:rkisp1 OR dfctx:rkisp1 OR s:rkisp1) AND NOT (s:"PATCH AUTOSEL" OR l:stable.vger.kernel.org) AND rt:5.year.ago..'
```
much better.
now you can open the [maildir](en.wikipedia.org/wiki/Maildir) with `neomutt -f ~/Mail/rksip1`

Now that we have that folder. We can keep track of new mail.
The command we typed (`lei -q`) will save that search (no need to hope it will stay in the cmd histroy)
run `lei ls-search` to see all saved commands:
```
dafna:~/git/media_tree$ lei ls-search
/home/dafna/Mail/rkisp1
```

There is a lei daeom 'lei-daemon' and another process called lei/store
it seems that saved searched are in /home/dafna/.local/share/lei/saved-searches

Let's say you reviewed and answered one some mails. Now you want to see your own answer in the maildir.
let's fetch again:
```
dafna:~/git/media_tree$ lei up ~/Mail/rkisp1/
# https://lore.kernel.org/all/ limiting to 2022-02-01 10:31 +0200 and newer
# /home/dafna/.local/share/lei/store 226/226
# /usr/bin/curl -Sf -s -d '' https://lore.kernel.org/all/?x=m&t=1&q=((dfn%3Adrivers%2Fmedia%2Fplatform%2Frockchip%2Frkisp1+OR+dfa%3Arkisp1+OR+dfb%3Arkisp1+OR+dfhh%3Arkisp1+OR+dfctx%3Arkisp1+OR+s%3Arkisp1)+AND+NOT+(s%3A%22PATCH+AUTOSEL%22+OR+l%3Astable.vger.kernel.org)+AND+rt%3A1486113446..)+AND+dt%3A20220201083127..
# https://lore.kernel.org/all/ 16/16
# 1 written to /home/dafna/Mail/rkisp1/ (242 matches)
```

And in mutt, type '$' to update (sync) with the new fetched mails.

So we basically don't need to be subscribed to mailing list anymore. We can download the mails we want
and reply to them.

3. scenario:
 - You want to track all new changes to the core API of your beloved subsystem. You care only about the core api changes and not specific drivers.
```
lei q -I https://lore.kernel.org/all/ -o ~/Mail/core-v4l2 --threads --dedupe=mid '(dfn:drivers/media/v4l2-core/ OR dfn:drivers/media/mc/ OR dfn:drivers/media/common/videobuf2/ ) AND NOT (s:"PATCH AUTOSEL" OR l:stable.vger.kernel.org) AND rt:1.month.ago..'
```
This is actually not perfect.

3. scenario:
- There is a new cool DMA api and you want to see who started using it:
If this api is really fresh new, you should not be affired of too many unrelevant mail,
```
lei q -I https://lore.kernel.org/all/ -o ~/Mail/dma-noncontig --threads --dedupe=mid dma_alloc_noncontiguous
```

# macros:
add the following macro to ~/.config/neomutt/mappings:

macro index p '<tag-pattern>~f patchwork@emeril.freedesktop.org<enter><tag-prefix-cond><purge-message><enter><sync-mailbox><enter>'

This is a macro that does:
1. Press T which bounds the <tag-prefix>
2. type `~f patchwork@emeril.freedesktop.org` which matches the "From" header.
3. pess ';' which will run a function on the tagged-messages
4. press 'D' to mark tagged messages for deletion
5. press $ to save changes - that is actually delete those email
