```
struct gen_pool {
	spinlock_t lock;
	struct list_head chunks;        /* list of chunks in this pool */
	int min_alloc_order;            /* minimum allocation order */
	genpool_algo_t algo;            /* allocation function */
	void *data;
	const char *name;
};

struct gen_pool_chunk {
	struct list_head next_chunk;	/* next chunk in pool */
	atomic_long_t avail;		/* the amount of available memory */
	phys_addr_t phys_addr;		/* physical starting address of memory chunk */
	void *owner;			/* private data to retrieve at alloc time */
	unsigned long start_addr;	/* start address of memory chunk */
	unsigned long end_addr;		/* end address of memory chunk (inclusive) */
	unsigned long bits[];		/* bitmap for allocating memory chunk */
};
```
so the bits array is a bitmap, each bit represent a minimal size of allocation (let's call it 'block')
as from 'min_alloc_order' input when createing the pool.
So for example, if minimum allocation is 4K then min_alloc_order = 12 and a block size is 4K.
Then if we add a chunk of size 2M then the number of bits in the bitmap
will be  2M / 4K = 2^(20-12) = 2^8 = 256.
So we have a bitmap of 256 bits. a zero bit means that the corrsponding block is free.
so we need to look for an area of zeroed bit in a sequence big enough for the allocation.
The size of the allocation is rounded up. So if someone want to allocate 5K then
in our case he will allocate round_up(5K, 4K) = 8K.
The round up is calculated in the line:
```
nbits = (size + (1UL << order) - 1) >> order;
```

# Simple usage example:
-----------------------
```
my_pool = gen_pool_create(int min_alloc_order, int nid);
rc = gen_pool_add(my_pool, addr, size, nid);
my_addr = gen_pool_alloc(my_pool , size);
gen_pool_free_owner(my_pool , addr, size, void **owner);
gen_pool_destroy(my_pool)
```

* gen_pool_create - initialize all,
```
spin_lock_init(&pool->lock);
INIT_LIST_HEAD(&pool->chunks);
pool->min_alloc_order = min_alloc_order;
pool->algo = gen_pool_first_fit;
pool->data = NULL;
pool->name = NULL;
```
* gen_pool_add
```
      chunk->phys_addr = phys;
        chunk->start_addr = virt;
        chunk->end_addr = virt + size - 1;
        chunk->owner = owner;
        atomic_long_set(&chunk->avail, size);

        spin_lock(&pool->lock);
        list_add_rcu(&chunk->next_chunk, &pool->chunks);
        spin_unlock(&pool->lock);
```
* gen_pool_alloc is then calling gen_pool_alloc_algo_owner:
```
cmpxchg(var_to_update, expected_val, new_val) - update a variable if the variable is what you expect it to be and anyway
return the old value of 'var_to_update'


add_owner
--------
size: size in bytes of the memory chunk
/*
 * num of alloc sizes that fits in the chunk
 * For example if minimum to allocate is 4K then min_alloc_order 12
 * and then nbits is the amount that 4K fits in 'size'
 * so we have a bit for each 4K
 */
unsigned long nbits = size >> pool->min_alloc_order; 

EXAMPLE size = 1G, order = 12 then `nbits = 2^(30-12) = 2^18 = 256K`

gen_pool_alloc_algo_owner(struct gen_pool *pool, size_t size, genpool_algo_t algo, void *data, void **owner)
------------------------------------------------------------------------------------------------------------

nbits = (size + (1UL << order) - 1) >> order;
EXAMPLE: say size is 3 * 4K
(= (3 * 4K + 4K - 1) >> 12 = (0x4000 - 1) >> 12 = 0x3fff >> 12 = 0x3
This gives the amount of 4K size we will have to allocate
 = * @nr: The number of zeroed bits we're looking for
end_bit = chunk_size(chunk) >> order; // this is not depended on the available space,
(= 2^18)

list_for_each_entry_rcu(chunk, &pool->chunks, next_chunk) {
	end_bit = chunk_size(chunk) >> order; (=256K)
	start_bit = algo(chunk->bits, end_bit, start_bit, nbits, data, pool, chunk->start_addr);