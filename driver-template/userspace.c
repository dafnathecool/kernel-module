/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include "major-minor.h"

static const char *mydevpath = "/dev/mydev";
static const char *kfifo_path = "/dev/mykfifo";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("mknod failed %d %d\n", rc, EEXIST);
		return rc;
	}
	return 0;
}

int main(int argc, char **argv)
{
	int fd, rc;

	rc = create_path(mydevpath, my_minor);
	if (rc)
		exit(EXIT_FAILURE);

	fd = open(mydevpath, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}
}
