#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <string.h>
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/sysmacros.h>
#include <linux/mman.h>
#include <errno.h>
#include "major-minor.h"

static const char *mydevpath = "/dev/mydev";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("mknod failed %d %d\n", rc, EEXIST);
		return rc;
	}
	return 0;
}

int main(int argc, char **argv)
{
	int fd, rc;
	unsigned int size[2] = {2, 1};
//	unsigned int size = 128;
	char *ptr[2];
	//int flags = MAP_HUGETLB | MAP_HUGE_2MB | MAP_SHARED | MAP_ANONYMOUS;
	int flags = MAP_HUGETLB | MAP_HUGE_2MB | MAP_PRIVATE | MAP_ANONYMOUS;
//	int flags = MAP_HUGETLB | MAP_PRIVATE | MAP_ANONYMOUS;
//	int flags = MAP_SHARED | MAP_ANONYMOUS;

	for (int i = 0; i < 2; i++) {
		size[i] *= 1024 * 1024 * 2;
		ptr[i] = mmap(NULL, size[i] , PROT_READ | PROT_WRITE, flags, -1, 0);

		if (ptr[i] == MAP_FAILED) {
			printf("mmap failed %u for ptr num %d %d\n", size[i], i, errno);
			perror("bla");
			return -1;
		}
	}

	//getchar();
	rc = create_path(mydevpath, my_minor);
	if (rc)
		exit(EXIT_FAILURE);

	fd = open(mydevpath, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	printf("ptr[0] %p size 0x%x\n", ptr[0], size[0]);
	//first we write the size 
	//second we write the pointer.
	//then the driver has all the info to call get_user_pages
	for (int i = 0; i < 2; i++) {
		rc = write(fd, &size[i], sizeof(size[i]));
		if (rc == -1) {
			perror("write 0");
			exit(EXIT_FAILURE);
		}
		rc = write(fd, &ptr[i], sizeof(ptr[i]));
		if (rc == -1) {
			perror("write 1");
			exit(EXIT_FAILURE);
		}
	}
	return 0;

}
