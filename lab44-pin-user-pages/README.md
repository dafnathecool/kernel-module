run `virtme-ng`:
```
~/git/kernel-module/lab44-pin-user-pages$ vng -v -r ~/git/kernel-module/kbuild/x86_64/ --rwdir /tmp --rwdir .
```

to enable huge pages mmap:

```
echo 20 | sudo tee /proc/sys/vm/nr_hugepages
```

insmod the driver:

```
sudo insmod driver.ko
```

now you can run:

```
sudo ./userspace
```
if you run it serverl times in a raw, at some point the command will fail with the error:
```
pin ptr 0x7f7929800000 failed (-14) tried to pin 512
```
Why do we get -EFAULT (-14) and not -ENOMEM ?


