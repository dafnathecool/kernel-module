#include "asm/uaccess.h"
#include "linux/mm.h"
#include "major-minor.h"
#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/delay.h>


#define MY_DEV_NAME "my-device"

#define SIZE 128

struct my_driver {
	struct platform_device *pdev;
};

static DEFINE_MUTEX(my_mutex);
static struct my_driver *mdriver;
static int idx;

struct user_pages {
	uint64_t userptr;
	unsigned int size;
	struct page **pages;
	u32 npages;
	struct list_head node;
};

static struct list_head userptr_list;

static int mycdrv_open(struct inode *inode, struct file *file)
{
	pr_debug("open device\n");
	idx = 0;
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	return 0;
}

static ssize_t mycdrv_write(struct file *file, const char __user *buf, size_t lbuf, loff_t *ppos)
{
	//unsigned char size;
	u64 start, end;
	u32 npages;
	struct page **pages;
	struct user_pages *p;
	unsigned int size;
	uint64_t userptr;
	int i, rc;

	pr_info("START write\n");
	/*
	* first user writes the size
	* second, user writes the pointer
	* once we have both, we can call get_user_pages
	*/
	switch (idx) {
	case 0:
		idx = 1;
		rc = get_user(size, (unsigned int __user *)buf);
		if (rc < 0)
			return rc;
		p = kmalloc(sizeof(*p), GFP_KERNEL);
		if (!p)
			return -ENOMEM;
		memset(p, 0, sizeof(*p));
		p->size = size;
		INIT_LIST_HEAD(&p->node);
		list_add(&p->node, &userptr_list);
		return sizeof(u64);
	case 1:
		p = list_first_entry(&userptr_list, struct user_pages, node);
		idx = 0;
		rc = get_user(userptr, (uint64_t __user *)buf);
		if (rc < 0)
			return rc;
		p->userptr = userptr;
		size = p->size;
		break;
	}

	pr_err("user pointer is - 0x%llx size: 0x%x\n", userptr, size);
	if (!access_ok((void __user *)(uintptr_t)userptr, size)) {
		pr_err("user pointer is invalid - 0x%llx 0x%x\n", userptr, size);
		return -EINVAL;
	}

	start = userptr & PAGE_MASK;
	end = PAGE_ALIGN(userptr + size);
	npages = (end - start) >> PAGE_SHIFT;
	pages = kvmalloc_array(npages, sizeof(struct page *), GFP_KERNEL);
	if (!pages)
		return -ENOMEM;

	rc = pin_user_pages_fast(start, npages, FOLL_WRITE | FOLL_LONGTERM, pages);

	if (rc != npages) {
		pr_err("pin ptr 0x%llx failed (%d) tried to pin %u\n", userptr, rc, npages);
		if (rc > 0) {
			unpin_user_pages(pages, rc);
			for (i = 0; i < npages ; i++) {
				rc = pin_user_pages_fast(start + i * PAGE_SIZE, 1,
					FOLL_WRITE | FOLL_LONGTERM, &pages[i]);
				if (rc < 0)
					pr_err("pin ptr failed (%d) for 1 page i = %d\n", rc, i);
			}
		}
		kvfree(pages);
	} else {
		pr_err("pin ptr 0x%llx success (%d) tried to pin %u pages %p-%p\n",
			userptr, rc, npages, pages[0], pages[npages - 1]);
		p->pages = pages;
		p->npages = npages;
	}

	return sizeof(u64);
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.write = mycdrv_write,
	.open = mycdrv_open,
	.release = mycdrv_release
};

static unsigned int count = 1;
static struct cdev my_cdev;

static void free_driver(void)
{
	kfree(mdriver);
	mdriver = NULL;
}

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	INIT_LIST_HEAD(&userptr_list);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_init(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);
	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	platform_set_drvdata(pdev, mdriver);
	dev_info(&pdev->dev,
		 "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static void my_remove(struct platform_device *pdev)
{
	static dev_t first;
	struct user_pages *p;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	list_for_each_entry(p, &userptr_list, node) {
		if (p->npages) {
			dev_dbg(&pdev->dev, "%s: bye to %u pages", __func__, p->npages);
			unpin_user_pages(p->pages, p->npages);
			kvfree(p->pages);
			p->pages = NULL;
		}
	}
	free_driver();
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static void empty(struct device *dev) { }

static struct platform_device my_device = {
	.dev.release = empty,
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;

	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
