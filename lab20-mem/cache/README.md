sources:
https://www.linuxjournal.com/article/7105
https://developer.arm.com/documentation/den0024/a/Caches
 

Terminology:
* **L1** - the cache closest to the cpu, (L2 is one further and so on untill the RAM)
* **Cache Line**  - the smallest unit of memory that can be transferred to or from a cache. The essential elements that quantify a cache are called the read and write line widths. Line width might differ between the different cache levels.
For Arm64 for example, the L1 cache line size is:
```
#define L1_CACHE_SHIFT          (6)
#define L1_CACHE_BYTES          (1 << L1_CACHE_SHIFT)
```
so 64 bytes. On x86 it seems to be configurable from `CONFIG_X86_L1_CACHE_SHIFT`.

* **Write trhough Cache** - when writing to the cache, the main mem is also written so that the cach and RAM are still consistent.
This is useful if there is small amount of write operations in compare to read operations. The drewback is that it data write will expereince latncy since we don't enjoy the advantages of the Cache.
* **Write Back Cache** - a write may be considered complete as soon as the data is stored in the cache. For a write back cache, as long as the written data is not transmitted, the cache line is considered dirty, because it ultimately must be written out to the level below. 
In other words, when wrting to the cache we don't imediately wirte to RAM (as oppose to Write through Cache) the.
* **Dirty cache line** - data is modified in the cache but not in main memory. When data is written to the cache, the Dirty bit
is turned on in that cache line to indicate that the data needs to be flush to the RAM at some point.
* **Stale Data** - Data that was written to main memory but not to the cache.
* **Cache Line interference** - this is an bug in which a dirty cache line was flushed to RAM which overwrites values that were updated
in the RAM (and not in the cache.
For example - cpu writes to address X in the cache. An extenal device writes to the corresponding address X+1 in the RAM.
Now the Cache line of addresse X,X+1 is flushed to RAM which overrides the value in X+1 which was written by the device.
* **Tag** - The tag is the part of a memory address stored within the cache that identifies the main memory address associated with a line of data. The top bits of the 64-bit address tell the cache where the information came from in main memory and is known as the tag.
* **Index** - The index is the part of a memory address that determines in which lines of the cache the address can be found.
* **set associative cache** - cache is divides to ways.
* **PIPT** - Physically Indexed, Physically Tagged.
* **inclusive cache model** - inclusive cache model, where the same data can be present in both the L1 and L2 caches. In an exclusive cache, data can be present in only one cache and an address cannot be found in both the L1 and L2 caches at the same time.

MT_NORMALE memory type - normal - weakly ordered + cached
MT_NORMALE_NC - non cacheable
MT_DEVICE_nGnRE -  strongly ordered + not cached

snoop Control Unit - SCU
Cache coherent Interconnect - 
shareability domain - typicaly all masters are in the same domain
bits 9:8 in the PTE
non-shareable - page can't be snooped for other clusters
inner-shareable - page can't be snooped for other clusters
outer-shareable - outer shareable domain
Linux always assume innter shareable

coherency options:
1. not cachable - simple in the cost of hardware performence
2. hw hardware coherehnce ACE 'AXI coherency extension' similar to 'cache coherent interconnect'
masters can snoop other masters.
3. coherency by cache maintainancy (the DMA streaming api) cache flush operations before other
external device.
or invalidated for the CPU after an external device touched the mem

in linux:
1. consistent dma: dma_alloc_coherent
2. streaming dma: map_single/sync_single etc.

dma_sync_device - cache flush opration for DMA_TO_DEVICE
dma_sync_device - cache flush_invalidation opration for DMA_FROM_DEVICE

dma-coherent on dt = Answer: From a ARM Linux perspective, yes! 
ACE axi feature?

if device is in the coherent domain


dma_pgprot - just normal memory and normal cachable

exynos MFC - codec

"IOMMU_CACHE" means the data is cachable in the device side ?
Answer: Depends on whether the device has a cache or not. ACE-Lite means no device side cache. ACE means even the device supports cache. The current example MFC is ACE-Lite. GPU like ARM- mali series have a cache in itself in which case transactions are cached at device side as well.


Dafna Hirschfeld
in the slide comapring the performance of the different API , I would expect the streamin api to be the fastest, or am I wrong?
Answer: If streaming API was the fastest, then there wouldn't have been necessity for snooping(H/w coherency), right? H/w coherency is the fastest. But yes, in the performance example, we expect streaming API to perform better than Non-cacheable memory, but for the number of frames we tested(500, 5000) the numbers came out favoring non-cacheable way. That's why Smitha mentioned profile the usecase multiple times before choosing the way of implementation. In general, following is preferable: H/w coherency >> S/w managed >> non-cacheable


oh, ok , interesting, so sometime non cachable might perform better than streaming api. You know if it depends on the number or frequency of frames?
Answer: Right, sometimes it performs better and it can depend on lot more factors like: cache line size, cache size, frequency of usage of shared buffer between CPU and device master, sequence of access to shared buffer between CPU and dev ice master, etc

so this ACE featrure is disabled when removing the 'dma-coherent' from the device tree? this feature can dymaically be enabled and disabled?
Answer: Good question :) This is how we started analyzing these stuff and the whole motivation behind dividing the session H/w vs S/w perspective. Any SOC with ARM MMU-500 or ARM MMU-600 compliant IOMMUs support ACE-Lite by default(and also ACE if device has internal cache and connected over CCI) So, from a H/W perspective if this much infrastructure is present, then we decide what to configure in software. If you use "dma-coherent" in DT, you are letting Linux system know that your H/W supports snooping. So yes, if you don' provide "dma-coherent" flag, ACE feature is not utilized even thought your device might still support it


Ajay Kumar
Dafna's question: so cache tagging operates only on physical addresses? ==> Depends on whether cache is Physically tagged or Virtually tagged(PIPT, VIVT). We have considered its PIPT for these examples.
