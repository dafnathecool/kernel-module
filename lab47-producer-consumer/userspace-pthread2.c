/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include <pthread.h>
#include "major-minor.h"

static const char *mydevpath = "/dev/mydev";
static const char *kfifo_path = "/dev/mykfifo";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("mknod failed %d %d\n", rc, EEXIST);
		return rc;
	}
	return 0;
}

// Function to be executed by the thread
void *consumer(void *arg)
{
	int fd = (int)arg;
	char prev = 'Z', val;
	int rc;
	int usec = 0;

	do {
		rc = read(fd, &val, sizeof(val));
		if (rc == -1) {
			if (errno == EBUSY) {
				printf("can't consume - buffer empty\n");
			} else {
				printf("fatal error %d\n", rc);
				exit(1);
			}
		} else if (val == 'a') {
			printf("read 'a' - BUG!!\n");
			exit(1);
		} else if (val > 'A' && val != prev + 1) {
			printf("read '%c' and %c- BUG!!\n", prev, val);
			exit(1);
		} else if (val == 'A' && prev != 'Z') {
			printf("read '%c' and %c- BUG!!\n", prev, val);
			exit(1);
		} else {
			printf("consumed '%c'\n", val);
			prev = val;
		}
		usec = (usec + 1) % 7;
		usleep(usec);
	} while (rc == 1 || (rc < 0 && errno == EBUSY));
	printf("consumer says BYE\n");
	return NULL;
}

int main(int argc, char **argv)
{
	pthread_t thread;
	int fd, rc;
	char a = 'A';
	int usec = 0;

	rc = create_path(mydevpath, my_minor);
	if (rc)
		exit(EXIT_FAILURE);

	fd = open(mydevpath, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}
	if (pthread_create(&thread, NULL, consumer, (void *)fd) != 0) {
		perror("Failed to create thread");
		return 1;
	}

	do {
		rc = write(fd, &a, sizeof(a));
		if (rc == 1) {
			a++;
			if (a > 'Z')
				a = 'A';
		}
		usec = (usec + 1) % 5;
		usleep(usec);
	} while (rc == 1 || (rc < 0 && errno == EBUSY));

	printf("producer got %d errno = %d\n", rc, errno);
	if (pthread_join(thread, NULL) != 0) {
		perror("Failed to join thread");
		return 1;
	}
	return rc;
}

