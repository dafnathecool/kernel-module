kmalloc
=======
implemented in include/linux/slab.h:
for large memory it will call kmalloc_large in linux/mm/slub.c

This eventually call `kmalloc_order` in linux/mm/slab_common.c
which does `alloc_pages` and return `page_address` of first page.
Note that `alloc_pages` returns a list of pages that are physically contiguous.
It then return `page_address` which is `lowmem_page_address` which is 'page_to_virt'
`page_address` and `lowmem_page_address` are defined in `include/linux/mm.h` and h
`page_to_virt` is defined in two places (for x86) - in page.h and in and in mm.h.
The implementation is eventually in mm.h:
```
#define page_to_virt(x) __va(PFN_PHYS(page_to_pfn(x)))
```
`page_to_pfn` is defined in asm-generic/memory_model.h:
```
#define __page_to_pfn(page)     (unsigned long)((page) - vmemmap)
```
`vmemmap` in arc/x86/include/asm/pgtable_64.h:
```
#define vmemmap ((struct page *)VMEMMAP_START)
unsigned long vmemmap_base __ro_after_init = __VMEMMAP_BASE_L4;
#define __VMEMMAP_BASE_L4       0xffffea0000000000UL
```

and `PFN_PHYS` defined in include/linux/pfn.h:
```
#define PFN_PHYS(x)     ((phys_addr_t)(x) << PAGE_SHIFT)
```
and finaly, `__va` in arc/x86/include/asm/page.h:
```
#define __va(x)                 ((void *)((unsigned long)(x)+PAGE_OFFSET))
```
where PAGE_OFFSET is 0xffff_8880_0000_0000
putting it all together:
`page_to_virt`:
```
__va(((phys_addr_t)(((unsigned long)((page) - ((struct page *)0xffffea0000000000UL))))) << PAGE_SHIFT)

((void *)((unsigned long)((((phys_addr_t)(((unsigned long)((page) - ((struct page *)0xffffea0000000000UL))))) << PAGE_SHIFT))+PAGE_OFFSET))




