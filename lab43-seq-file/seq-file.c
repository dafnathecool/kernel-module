#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/seq_file.h>


#define MY_DEV_NAME "my-device"

#define SIZE 128

struct my_driver {
	struct platform_device *pdev;
};

static struct my_driver *mdriver;

static void *ct_seq_start(struct seq_file *s, loff_t *pos)
{
	loff_t *spos = kmalloc(sizeof(loff_t), GFP_KERNEL);

	if (!spos)
		return NULL;
	*spos = *pos;
	return spos;
}

static void *ct_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
	loff_t *spos = v;
	*pos = ++*spos;
	return spos;
}

static void ct_seq_stop(struct seq_file *s, void *v)
{
	kfree(v);
}

static int ct_seq_show(struct seq_file *s, void *v)
{
	loff_t *spos = v;

	seq_printf(s, "%lld\n", (long long)*spos);
	return 0;
}

static const struct seq_operations ct_seq_ops = {
	.start = ct_seq_start,
	.next  = ct_seq_next,
	.stop  = ct_seq_stop,
	.show  = ct_seq_show
};

static int ct_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &ct_seq_ops);
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	return 0;
}

static ssize_t mycdrv_read(struct file *file, char __user *buf, size_t lbuf, loff_t *ppos)
{
	pr_info("START READ\n");
	return 0;
}

static const struct file_operations ct_file_ops = {
	.owner   = THIS_MODULE,
	.open    = ct_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = seq_release
};

static unsigned int count = 1;
static int my_major = 455, my_minor;
static struct cdev my_cdev;

static void free_driver(void)
{
	kfree(mdriver);
	mdriver = NULL;
}

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &ct_file_ops);

	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);
	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	platform_set_drvdata(pdev, mdriver);
	dev_info(&pdev->dev,
		 "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static void my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static void empty(struct device *dev) { }

static struct platform_device my_device = {
	.dev.release = empty,
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;

	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
