#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>

#define MY_DEV_NAME "my-device"

#define SIZE 128

static unsigned int alloc_method = 0;
module_param(alloc_method, uint, 0000);
MODULE_PARM_DESC(alloc_method, "0=kmalloc, 1=attrs, 2=get-free-pages");

struct my_driver {
	struct platform_device* pdev;
	char *buf;
	dma_addr_t dma_addr;
};

static DEFINE_MUTEX(my_mutex);
static struct my_driver* mdriver = NULL;

#define KM 0
#define VM 1

struct bufs {
	char *bufs_arr[1000];
	int idx = 0;
}

struct bufs my_bufs[2];

/*
	vbuf = vmalloc(100);
	pr_err("vbuf 0x%08lx\n", (unsigned long)vbuf);
	pr_err("is_vmalloc_addr(vbuf) %d\n", is_vmalloc_addr(vbuf));
	pr_err("virt_addr_valid(vbuf) %d\n", virt_addr_valid(vbuf));
	pr_err("virt_to_phys(vbuf): 0x%08llx\n", virt_to_phys(vbuf));
	pr_err("page_to_phys(virt_to_page(vbuf)): 0x%08llx\n",
		page_to_phys(virt_to_page(vbuf)));
	pr_err("page_to_phys(vmalloc_to_page(vbuf)): 0x%08llx\n\n",
		page_to_phys(vmalloc_to_page(vbuf)));
*/


long my_ioctl(struct file *file, unsigned int cmd, unsigned long size)
{
	void *buf;
	struct bufs *b;
	switch(cmd) {
		case KM:
			pr_err("kmalloc(%lu)\n", size);
			buf = kmalloc(size, GFP_KERNEL);
		
			break;
		case VM:
			pr_err("vmalloc(%lu)\n", size);
			buf = vmalloc(size);
			break;
		default:
			pr_err("bad cmd %d\n", cmd);
			return -1;
	}
	if (!buf) {
		pr_err("failed\n");
		return -ENOMEM;
	}
	b = &my_bufs[cmd]
	b->bufs_arr[b->idx++] = buf;
		

	return 0;
}


static int mycdrv_open(struct inode *inode, struct file *file)
{
	pr_err("OPEN device\n");
	mutex_lock(&my_mutex);
	/* can mdriver be NULL here ? */
	file->private_data = mdriver ? mdriver->pdev : NULL;
	mutex_unlock(&my_mutex);
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	int j;

	file->private_data = NULL;
	for(j = 0; j < kidx; j++)
		kfree(kbufs[j]);
	kidx = 0;
	for(j = 0; j < vidx; j++)
		vfree(vbufs[j]);
	vidx = 0;

	return 0;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .open = mycdrv_open,
        .release = mycdrv_release,
	.unlocked_ioctl = my_ioctl
};

static unsigned int count = 1;
static int my_major = 455, my_minor = 0;
static struct cdev my_cdev;

static void free_driver(void)
{
	mutex_lock(&my_mutex);
	if (mdriver) {
		kfree(mdriver);
		mdriver = NULL;
	}
	mutex_unlock(&my_mutex);
}

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_err(&pdev->dev, "%s: driver probed!!\n", __func__);
	dev_err(&pdev->dev, "%s: PAGE_OFFSET=%lx\n", __func__, PAGE_OFFSET);
	dev_err(&pdev->dev, "%s: PAGE_END=%lx\n", __func__, PAGE_END);
	dev_err(&pdev->dev, "%s: PAGE_SIZE=%lx\n", __func__, PAGE_SIZE);
	dev_err(&pdev->dev, "%s: PHYS_OFFSET=%llx\n", __func__, PHYS_OFFSET);
	dev_err(&pdev->dev, "%s: driver probed!!\n", __func__);
	dev_err(&pdev->dev, "%s: PAGE_OFFSET=%lx\n", __func__, PAGE_OFFSET);
	//PAGE_END seems not to be defined on x86
	//dev_err(&pdev->dev, "%s: PAGE_END=%lx\n", __func__, PAGE_END);
	dev_err(&pdev->dev, "%s: PAGE_SIZE=%lx\n", __func__, PAGE_SIZE);
	//dito, no PHYS_OFFSET on x86
	//dev_err(&pdev->dev, "%s: PHYS_OFFSET=%llx\n", __func__, PHYS_OFFSET);


	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_lock(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);

	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;
	mutex_unlock(&my_mutex);

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}

	platform_set_drvdata(pdev, mdriver);
	/*
	dev_err(&pdev->dev, "%s: == dma_alloc_coherent ==\n", __func__);
	dma_alloc_coherent(&pdev->dev, 4096, &d, GFP_KERNEL);
	dev_err(&pdev->dev, "%s: == dma_alloc_noncoherent ==\n", __func__);
	dma_alloc_noncoherent(&pdev->dev, 4096, &d, DMA_BIDIRECTIONAL, GFP_KERNEL);
	*/
	dev_info(&pdev->dev, "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static int __init platform_driver_init(void)
{
	int ret;

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
