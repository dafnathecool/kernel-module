
struct vm_struct {
	struct vm_struct	*next;
	void			*addr;
	unsigned long		size;
	unsigned long		flags;
	struct page		**pages;
#ifdef CONFIG_HAVE_ARCH_HUGE_VMALLOC
	unsigned int		page_order;
#endif
	unsigned int		nr_pages;
	phys_addr_t		phys_addr;
	const void		*caller;
};

struct vmap_area {
	unsigned long va_start;
	unsigned long va_end;

	struct rb_node rb_node;         /* address sorted rbtree */
	struct list_head list;          /* address sorted list */

	/*
	 * The following two variables can be packed, because
	 * a vmap_area object can be either:
	 *    1) in "free" tree (root is free_vmap_area_root)
	 *    2) or "busy" tree (root is vmap_area_root)
	 */
	union {
		unsigned long subtree_max_size; /* in "free" tree */
		struct vm_struct *vm;           /* in "busy" tree */
	};
};
vmalloc -> __vmalloc_node(size, 1, GFP_KERNEL, NUMA_NO_NODE,
                                __builtin_return_address(0));

__vmalloc_node(size, 1, GFP_KERNEL, NUMA_NO_NODE,
                                __builtin_return_address(0));

# This is the big function with all the code
return __vmalloc_node_range(size, align, VMALLOC_START, VMALLOC_END,
                                gfp_mask, PAGE_KERNEL, 0, node, caller);

========================================================
========== This part is only for the virtual mem =======
It finds a virtual mem that is big enough, it has nothing with the physical mem
===================
	struct vm_struct *area = __get_vm_area_node(real_size, align, shift, VM_ALLOC |
                                  VM_UNINITIALIZED | vm_flags, start, end, node,
                                  gfp_mask, caller);
		 area = kzalloc_node(sizeof(*area), gfp_mask & GFP_RECLAIM_MASK, node);
		 va = alloc_vmap_area(size, align, start, end, node, gfp_mask);
			static struct vmap_area *alloc_vmap_area(size, align, vstart, vend, node, ggfp_mask)
				struct vmap_area va = kmem_cache_alloc_node(vmap_area_cachep, gfp_mask, node);
				//looks for a vm with the restrictions in free_vmap_area_root.rb_node;
				addr = __alloc_vmap_area(size, align, vstart, vend);
				va->va_start = addr;
				va->va_end = addr + size;
				va->vm = NULL;
				insert_vmap_area(va, &vmap_area_root, &vmap_area_list);
				return va;
		setup_vmalloc_vm(area, va, flags, caller); // this does va->vm = vm, area->addr = va->va_start;
		retrun area;
================================
========= here we take the virtual mem we found and build the page table to map it to the phyisical mem
================================

        addr = __vmalloc_area_node(area, gfp_mask, prot, shift, node);
			area->pages = kmalloc_node(array_size, nested_gfp, node);
			//this fills the area->pages with allocated physical pages.
			area->nr_pages = vm_area_alloc_pages(gfp_mask, node, page_order, nr_small_pages, area->pages);
			vmap_pages_range(addr, addr + size, prot, area->pages, page_shift
				vmap_pages_range_noflush(addr, end, prot, pages, page_shift)
					vmap_small_pages_range_noflush(addr, end, prot, pages);
						vmap_pages_p4d_range(pgd, addr, next, prot, pages, &nr, &mask);
							vmap_pages_pud_range(p4d, addr, next, prot, pages, nr, mask)
								vmap_pages_pmd_range(pud, addr, next, prot, pages, nr, mask)
									vmap_pages_pte_range(pmd, addr, next, prot, pages, nr, mask
										do {
											struct page *page = pages[*nr];
											set_pte_at(&init_mm, addr, pte, mk_pte(page, prot));
											(*nr)++;
										} while (pte++, addr += PAGE_SIZE, addr != end);
			return area->addr;


========================================================================
ioremap
========================================================================
#define ioremap(addr, size)             __ioremap((addr), (size), __pgprot(PROT_DEVICE_nGnRE))
void __iomem *__ioremap(phys_addr_t phys_addr, size_t size, pgprot_t prot)
{
        return __ioremap_caller(phys_addr, size, prot, __builtin_return_address(0));
}
static void __iomem *__ioremap_caller(phys_addr_t phys_addr, size_t size, pgprot_t prot, void *caller)
	area = get_vm_area_caller(size, VM_IOREMAP, caller); //this part has nothing to do with the phyisical address, it is just used to get an available virtual area
        addr = (unsigned long)area->addr;
        area->phys_addr = phys_addr;
	ioremap_page_range(addr, addr + size, phys_addr, prot);
		vmap_range(addr, end, phys_addr, prot, iomap_max_page_shift);
			vmap_range_noflush(addr, end, phys_addr, prot, max_page_shift);
				vmap_p4d_range(pgd, addr, next, phys_addr, prot, max_page_shift, &mask);
					vmap_pud_range(p4d, addr, next, phys_addr, prot, max_page_shift, mask)
						vmap_pmd_range(pud, addr, next, phys_addr, prot, max_page_shift, mask)
							vmap_pte_range(pmd, addr, next, phys_addr, prot, max_page_shift, mask)
								pfn = phys_addr >> PAGE_SHIFT;
								pte = pte_alloc_kernel_track(pmd, addr, mask);
								do {
									set_pte_at(&init_mm, addr, pte, pfn_pte(pfn, prot));
									pfn++;
								} while (pte += PFN_DOWN(size), addr += size, addr != end);
