VFIO
====
https://vfio.blogspot.com/

This is the flow:
================
dma_alloc_attrs ->
	get_dma_ops:
	if (dev->dma_ops)
		return dev->dma_ops
	return get_arch_dma_ops

lets see what are dma_ops,
They are set in 'iommu_setup_dma_ops' or in 'set_dma_ops' but 'set_dma_ops' is used only for arm and unreateld drivers

Now we have:


platform_bus_type = {
..
.dma_configure = platform_dma_configure
};


platform_dma_configure ->
of_dma_configre ->
of_dma_configure_id ->
	This is the function that checks if the iommu is configured by calling 'iommu = of_iommu_configure' at the end we have:
	arch_setup_dma_ops(dev, dma_start, size, iommu, coherent);

And then in the probe we have:
=============================
really_probe:

dev->bus->dma_configure

so 'iommu_setup_dma_ops' in dma-iommu.c sets the ops to iommu_dma_ops


iommu_dma_alloc -> iommu_dma_alloc_remap
===============

```
static void *iommu_dma_alloc_remap(struct device *dev, size_t size,
                dma_addr_t *dma_handle, gfp_t gfp, pgprot_t prot,
                unsigned long attrs)
{                       
        struct page **pages;
        struct sg_table sgt;
        void *vaddr;
                
        pages = __iommu_dma_alloc_noncontiguous(dev, size, &sgt, gfp, prot,
                                                attrs);
        if (!pages)
                return NULL;
        *dma_handle = sgt.sgl->dma_address;
        sg_free_table(&sgt);
        vaddr = dma_common_pages_remap(pages, size, prot,
                        __builtin_return_address(0));
        if (!vaddr)
                goto out_unmap;
        return vaddr;

out_unmap:
        __iommu_dma_unmap(dev, *dma_handle, size);
        __iommu_dma_free_pages(pages, PAGE_ALIGN(size) >> PAGE_SHIFT);
        return NULL;
}
```

