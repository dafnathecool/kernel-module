(return 0 2>/dev/null) || (echo "the script must be sourced" && exit 1)

export AGAINST_SRC=1
export CROSS_COMPILE=x86_64-linux-gnu-
export GIT_PATH=/home/dafna/git/kernel-module
export KBUILD_OUTPUT=$GIT_PATH/kbuild/x86_64
export MODDIR=$GIT_PATH/modules
export ARCH=x86_64
