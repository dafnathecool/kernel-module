#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/workqueue.h>
#include <linux/delay.h>

#define MY_DEV_NAME "my-device16"

#define SIZE 128

static DEFINE_MUTEX(my_mutex);

struct my_driver {
	struct platform_device* pdev;
	struct workqueue_struct *my_workqueue;
	struct work_struct my_work;
	unsigned int works_cnt;
};

static void my_work_func(struct work_struct *work)
{
	struct my_driver *mdriver = container_of(work, struct my_driver, my_work);
	mdriver->works_cnt++; //todo: lock this

	pr_err("had %u works\n", mdriver->works_cnt);
	msleep(1000);
	pr_err("after sleep: had %u works\n", mdriver->works_cnt);
}
	

static struct my_driver* mdriver = NULL;

static int mycdrv_open(struct inode *inode, struct file *file)
{
	int i;
	int ret;

	pr_err("OPEN device\n");
	mutex_lock(&my_mutex);
	/* can mdriver be NULL here ? */
	file->private_data = mdriver ? mdriver->pdev : NULL;
	mutex_unlock(&my_mutex);
	for(i = 0; i < 100; i++) {
		ret = queue_work(mdriver->my_workqueue, &mdriver->my_work);
		pr_err("%d %d\n", i ,ret);
	}
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device good\n");

	file->private_data = NULL;
	return 0;
}


static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .open = mycdrv_open,
        .release = mycdrv_release,
	//.mmap = mycdrv_mmap
};

static unsigned int count = 1;
static int my_major = 416, my_minor = 0;
static struct cdev my_cdev;

static void free_driver(void)
{
	mutex_lock(&my_mutex);
	if (mdriver) {
		kfree(mdriver);
		mdriver = NULL;
	}
	mutex_unlock(&my_mutex);
}


static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_lock(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);

	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;
	mutex_unlock(&my_mutex);

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	//mdriver->my_workqueue = alloc_ordered_workqueue("Tuli", WQ_MEM_RECLAIM | WQ_FREEZABLE);
	mdriver->my_workqueue = alloc_workqueue("Tuli Hatuli", WQ_MEM_RECLAIM, 0);

	if (!mdriver->my_workqueue) //todo: do freeing stuff here
		return -ENOMEM;
	INIT_WORK(&mdriver->my_work, my_work_func);

	platform_set_drvdata(pdev, mdriver);
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static struct platform_device my_device = {
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;
	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
