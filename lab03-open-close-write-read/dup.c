#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
	int fd1 = open("/dev/lab3", O_RDWR);
	if (fd1 < 0) {
		perror("xx");
		return fd1;
	}
	int n = write(fd1, "123", sizeof("123"));
	if (n < sizeof("123")) {
		printf("xxxn=%d\n", n);
		return n;
	}

	int fd2 = dup(fd1);
	if (fd2 < 0) {
		perror("sdfsdf");
		return fd1;
	}
	close(fd1);
	write(fd2, "bla", sizeof("bla"));
	sleep(10);
	close(fd2);

	printf("done\n");
	return 0;
}