This is what I did:

(pinchartl)dafna@guri:~/git/linux$ make KBUILD_OUTPUT=~/git/build/x86_64 INSTALL_MOD_PATH=~/git/modules modules_install
(pinchartl)dafna@guri:~/git/linux$ virtme-run --kimg ../build/x86_64/arch/x86_64/boot/bzImage --moddir ~/git/modules/
(pinchartl)dafna@guri:~/git/linux$ ../virtme/virtme-configkernel --defconfig
(pinchartl)dafna@guri:~/git/linux$ mv .config ../build/x86_64/
(pinchartl)dafna@guri:~/git/linux$ make ARCH=x86_64 KBUILD_OUTPUT=~/git/build/x86_64 -j9

(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ export ARCH=x86_64
(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ export KERNELDIR=/home/dafna/git/linux
(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ export KBUILD_OUTPUT=/home/dafna/git/build/x86_64
(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ export AGAINST_SRC=1
(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ make

(master)dafna@guri:~/git/kernel-module/lab03-open-close-write-read$ virtme-run --kimg ~/git/build/x86_64/arch/x86_64/boot/bzImage --moddir ~/git/modules/ --pwd

root@(none):/home/dafna/git/kernel-module/lab03-open-close-write-read# insmod lab3-open-close-read-write.ko

till now we loaded the driver but not the device. The device is in lab01:

(master)dafna@guri:~/git/kernel-module/lab01-platform-device$ make

now we can load the device:
root@(none):/home/dafna/git/kernel-module/lab03-open-close-write-read# insmod ../lab01-platform-device/lab1-platform-device.ko
root@(none):/home/dafna/git/kernel-module/lab03-open-close-write-read# mknod /dev/lab3 c 455 0

