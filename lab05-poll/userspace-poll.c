/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include "major-minor.h"


static const char *waitqueue_path = "/dev/mywaitqueue";
static const char *kfifo_path = "/dev/mykfifo";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("%d %d\n", rc, EEXIST);
		perror("mknod failed");
		return rc;
	}
	return 0;
}

int main(int argc, char **argv) {
	char c;
	int fd, i, n, rc;
	short revents;
	struct pollfd pfd;

	rc = create_path(waitqueue_path, my_waitqueue_minor);
	if (rc)
		exit(EXIT_FAILURE);

	rc = create_path(kfifo_path, my_kfifo_minor);
	if (rc)
		exit(EXIT_FAILURE);
	fd = open(kfifo_path, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	pfd.fd = fd;
	pfd.events = POLLIN;
	while (1) {
		puts("poll");
		i = poll(&pfd, 1, -1);
		if (i == -1) {
			perror("poll");
			assert(0);
		}
		revents = pfd.revents;
		printf("revents = %d\n", revents);
		if (revents & POLLIN) {
			n = read(pfd.fd, &c, 1);
			printf("POLLIN %c\n", c);
		}
	}
}
