1. install the module
2. run in one shell or in the background the polling process ./upoll, you might need to run with sudo
3. make sure the char file /dev/mywaitqueue is created with major 500.
3. in a different shell, (or same shell if `upoll` is in background) do `echo -n F | sudo tee /dev/mywaitqueue`
note: the '-n' option omit the trailing '\n' in 'echo' so the driver is not overwritten with '\n'
