/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <linux/errno.h> /* EFAULT */
#include <linux/fs.h>
#include <linux/kernel.h> /* min */
#include <linux/module.h>
#include <linux/poll.h>
#include <linux/printk.h> /* printk */
#include <linux/uaccess.h> /* copy_from_user, copy_to_user */
#include <linux/wait.h> /* wait_queue_head_t, wait_event_interruptible, wake_up_interruptible  */
#include <uapi/linux/stat.h> /* S_IRUSR */
#include <linux/platform_device.h>
#include <linux/cdev.h>
#include <linux/kfifo.h>
#include "major-minor.h"

static wait_queue_head_t waitqueue;
spinlock_t my_fifo_lock;
static struct kfifo my_fifo;

#define MY_DEV_NAME "my-kfifo-device"

/*
The semantics for the copy_to_user() and copy_from_user functions are the same as for the familiar memcpy()
function, except that the return value is the number of bytes not successfully transferred. Thus if we want to transfer
nbytes , the actual value transferred would be:
bytes_transferred = nbytes - copy_to_user (ubuf, kbuf, nbytes);
*/
static ssize_t read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
	int copied = 0;

	pr_info("%s: called\n", __func__);
	if (kfifo_to_user(&my_fifo, buf, len, &copied)) {
		pr_info("%s: copy failed\n", __func__);
		return -EFAULT;
	}
	return copied;
}

static ssize_t write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
	int copied = 0;

	pr_info("%s: called\n", __func__);
	if (kfifo_from_user(&my_fifo, buf, len, &copied)) {
		pr_info("%s: copy failed\n", __func__);
		return -EFAULT;
	}

	if (copied) {
		pr_info("%s: called - wake up with %d new elements\n", __func__, copied);
		wake_up(&waitqueue);
	}
	return copied;
}

unsigned int poll(struct file *filp, struct poll_table_struct *wait)
{
	poll_wait(filp, &waitqueue, wait);
	if (!kfifo_is_empty(&my_fifo)) {
		pr_info("%s: return POLLIN - we have %d elements\n", __func__, kfifo_len(&my_fifo));
		return POLLIN;
	} else {
		pr_info("%s: kfifo is empty\n", __func__);
	}
	return 0;
}

static const struct file_operations fops = {
	.owner = THIS_MODULE,
	.read = read,
	.write = write,
	.poll = poll
};

static struct cdev my_cdev;
static unsigned int count = 1;


static int __init myinit(void)
{
	dev_t first;
	int ret;

	/* allocate kfifo, of 16 elements, each element of the size of one byte */
	ret = kfifo_alloc(&my_fifo, 16, GFP_KERNEL);
	if (ret)
		return ret;
	spin_lock_init(&my_fifo_lock);
	pr_info("kfifo allocated, is initialized: %s, esize = %u\n",
				kfifo_initialized(&my_fifo) ? "True" : "False", kfifo_esize(&my_fifo));

	first = MKDEV(my_major, my_kfifo_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		kfifo_free(&my_fifo);
		return ret;
	}

	cdev_init(&my_cdev, &fops);
	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		kfifo_free(&my_fifo);
		pr_err("%s: cdev_add failed (%d)\n", __func__, ret);
		return ret;
	}

	pr_info("%s: registered on major %u and minor %d\n", __func__, my_major, my_kfifo_minor);
	init_waitqueue_head(&waitqueue);
	return 0;
}

static void myexit(void)
{
	dev_t first = MKDEV(my_major, my_waitqueue_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	kfifo_free(&my_fifo);
}

module_init(myinit)
module_exit(myexit)
MODULE_LICENSE("GPL");
