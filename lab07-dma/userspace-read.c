#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>


#define SIZE 64
char buf[SIZE];

int main(int argc, char *argv[])
{
	ssize_t ret = 0;
	char *area;
	char *area2;
	size_t size = 128;

	if (argc != 3) {
		printf("usage: %s <dev file name> <read buf size>\n", argv[0]);
		return -1;
	}
	int count = atoi(argv[2]);
	if (count > SIZE || count <= 0) {
		printf("bad buffer size given, max is %u\n", SIZE);
		return -1;
	}
	
	int fd = open(argv[1], O_RDWR);
	if (fd < 0) {
		perror("open faild\n");
		area = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
		if (area == MAP_FAILED) {
			printf("basa!\n");
			return -1;
		}
		printf("XXX area is %lx\n", (long unsigned int)area);
		getchar();
		area = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
		if (area == MAP_FAILED) {
			printf("basa!\n");
			return -1;
		}
		printf("XXX area is %lx\n", (long unsigned int)area);
		getchar();
		return -1;
	}
	getchar();
	area = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, fd, 0);
	if (area == MAP_FAILED) {
		printf("basa!\n");
		return -1;
	}
	getchar();
	area2 = mmap(NULL, size,  PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (area2 == MAP_FAILED) {
		printf("basa!\n");
		return -1;
	}
	getchar();
	//printf("done 0x%08lx\n", (long unsigned int)area);
	area[0] = 'd';
	area[1] = 'a';
	area[2] = 'f';
	area[3] = 'n';
	area[4] = 'a';
	area[5] = 0;
	printf("XXX area is %lx\n", (long unsigned int)area);
	printf("XXX done2 0x%08lx %s XXX\n", (long unsigned int)area2, area2);
	//printf("done 0x%08lx\n %c%c", (long unsigned int)area2, area[0], area[1]);
	return 0;
}
		
	
