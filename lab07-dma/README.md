```
scp /home/dafna/git/kbuild/elm/drivers/media/platform/lab7-dma.ko root@10.42.0.175:/lib/modules/5.13.0-rc3/kernel/drivers/media/platform/
modprobe lab1-platform-device && modprobe lab7-dma
mknod /dev/lab7 c 455 0
```

To compile to virtme on my laptop:
```
cd ~/git/media_tree/
source ~/git/media_tree/x86-make.sh
make modules_prepare
cd -
make KROOT=/home/dafna/git/media_tree/ MODDIR=/home/dafna/git/media_tree/tmp
```

#### DMA and linux-media

I am currently diving into the DMA API and how is it used in linux-media.
As a usage example, I worte a simple dirver and userspace in lab7-dma that
demonstrate dma_alloc_attrs and how to mmap it to userspace.
In the simple case there is no iommu and then the dma code in `kernel/dma` will
flow the path of `if(dma_alloc_direct)` / `if(dma_map_direct)`

# Things that are still not clear:

* Q: dma_alloc_attrs is not neccessarily coherent, then how come dma_alloc_coherent just calls `dma_alloc_attrs`
* A: dma_alloc_attrs is actually is coherent

* Q: is it possible to map to userspace a buffer allocated with kalloc?
* A: yes! see: https://stackoverflow.com/questions/10760479/how-to-mmap-a-linux-kernel-buffer-to-user-space/10770582#10770582

* Q: compare the fllow of `dma_alloc_coherent` and `dma_alloc_noncoherent` to understand the difference.
* A: the dma_alloc_noncoherent is broken, so this question is obsolete. It is indeed used only 8 times in  the whole kernel.
https://lwn.net/Articles/732107/

* Q: what is the diffference between the `mem_ops`: `vb2_dma_contig_memops` and `vb2_dma_sg_memops` ?
* A: the contig version uses the coherent api dma_alloc_attrs
   the sg version return a list of dma addresses that should be configured to a mmu or something like that.

* Q: what will happen in rkisp1 if I replace `vb2_dma_contig_memops` with `vb2_dma_sg_memops` ?
* A: I tried it and it worked

* Q: what will happen in vimc if I replace `vb2_vmalloc_memops` with `vb2_dma_contig_memops` ?
* A: I tried it and it worked

=============

* Q: what will happen if I will not allocate the dummy buffer in rkisp1 and instead put null ?
* Q: What is the behaviour of dma when the device has a reserved memory node ? Is the memory taken from there ?
* Q: What happen if the device is under `iommu` ? does `dma_alloc_attr` calls `ops->alloc` instead of `dma_direct_alloc` ?
* Q: understand how iommu api works
* Q: In the patchset: [PATCHv3 0/8] videobuf2: support new noncontiguous DMA API
in patch 8: https://patchwork.kernel.org/project/linux-media/patch/20210709092027.1050834-9-senozhatsky@chromium.org/
What is the use of flush_kernel_vmap_range(buf->vaddr, buf->size);

change to dma-contig in videobuf2 stream api:
https://gitlab.collabora.com/dafna/linux/-/commit/bb936886207931e197622ebe5dd6ef139c2af7a4
