/* https://cirosantilli.com/linux-kernel-module-cheat#irq-ko */

#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>


#define NAME "lkmc_irq"
#define MAX_IRQS 256

static int count[MAX_IRQS];
static int irqs[MAX_IRQS];
static int idx;

static int *my_cookie_ptr;
#define COOKIE 0x12345
/**
 * Return value from kernel docs:*
 *
 * enum irqreturn
 * @IRQ_NONE        interrupt was not from this device or was not handled
 * @IRQ_HANDLED     interrupt was handled by this device
 * @IRQ_WAKE_THREAD handler requests to wake the handler thread
 */
static irqreturn_t handler(int irq, void *cookie)
{
	//pr_debug("handler irq = %d dev = %d\n", irq, *(int *)dev);
	count[irq]++;

	/* print only every 200 irqs */
	idx = (idx + 1) % 200;
	if (idx == 0) {
		int i;
		for(i = 0; i < MAX_IRQS ; i++)
			if (!irqs[i])
				pr_debug("0x%lx: irq %d: %d times\n", (unsigned long)cookie, i, count[i]);
	}
	return IRQ_NONE;
}


static int myinit(void)
{
	int ret, i;

	my_cookie_ptr = kmalloc(256, GFP_KERNEL);
	if (!my_cookie_ptr)
		return -ENOMEM;
	for (i = 0; i < MAX_IRQS; ++i) {
		ret = request_irq( i, handler, IRQF_SHARED, "myirqhandler0", (void *)my_cookie_ptr);
		irqs[i] = ret;
		pr_info("request_irq irq = %d ret = %d\n", i, ret);
	}
	return 0;
}

static void myexit(void)
{
	int i;


	for (i = 0; i < MAX_IRQS; ++i) {
		if (!irqs[i]) {
			free_irq(i, (void *)my_cookie_ptr);
		}
	}
	kfree(my_cookie_ptr);
}

module_init(myinit)
module_exit(myexit)
MODULE_LICENSE("GPL");
