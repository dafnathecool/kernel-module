/* https://cirosantilli.com/linux-kernel-module-cheat#irq-ko */

#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include <linux/delay.h>

static DEFINE_MUTEX(my_mutex);

struct my_data {
	struct kref refcount;
	int bla;
};

static void data_release(struct kref *ref)
{
	struct my_data *data = container_of(ref, struct my_data, refcount);
	kfree(data);
}

static int more_data_handling(void *cb_data)
{
	struct my_data *data = cb_data;
	msleep(1000);
	mutex_lock(&my_mutex);
	kref_get(&data->refcount);
	mutex_unlock(&my_mutex);
	data->bla = 0x5566;
	kref_put(&data->refcount, data_release);
	return 0;
}

static int myinit(void)
{

	int rv = 0;
	struct my_data *data;
	struct task_struct *task;
	data = kmalloc(sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;
	kref_init(&data->refcount);
	data->bla = 0x1122;

	task = kthread_run(more_data_handling, data, "more_data_handling");
	if (task == ERR_PTR(-ENOMEM)) {
		rv = -ENOMEM;
		goto out;
	}

	data->bla = 0x3344;
out:
	mutex_lock(&my_mutex);
	kref_put(&data->refcount, data_release);
	mutex_unlock(&my_mutex);
	return rv;
	return 0;
}

static void myexit(void)
{
}

module_init(myinit)
module_exit(myexit)
MODULE_LICENSE("GPL");
