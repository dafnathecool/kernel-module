To corss compile to arm64:

Compilation
===========
1. need to install the toolchain for corss compile `sudo apt install gcc-aarch64-linux-gnu`
2. `make ARCH=arm64 defconfig`
3. `make ARCH=arm64 menuconfig` - on the meunu with should uncheck all SoC that we are not going
to use and keep only the SoC we compile for (rockchip in this case) otherwise we compile too
many things that we don't actually need.
4. then we compile with: 
`make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE=aarch64-linux-gnu- -j9`

Kernel Loading
==============
There are various ways to load the kernel and the module to the board you have. For example
copy it to and sd card. In this tutorial we will use tftp which is loading the kernel
to the board's memory through ethernet cable.

1. 
