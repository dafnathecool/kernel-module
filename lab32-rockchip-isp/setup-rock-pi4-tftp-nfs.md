## compiling the kernel:
on mainline linux:
```
make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" defconfig
make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" menuconfig
```
Here we can set the SoC to rockchip to avoid redundant compiling for other SoCs.
For NFS, all modules comes from remote fs. It is therefore important that the ethernet
driver will NOT be compiled as module. so set 'CONFIG_DWMAC_ROCKCHIP=y'.
In addition, to use panfrost, set 'CONFIG_DRM_PANFROST=m'.
Then compile:
```
make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" -j9
```

## set the ethernet
once an ethernet cable is connected between the rock-pi4 and the laptop, you want the connection
to be shared and you want an ip be assign to it.
This is done with the explanation here: https://askubuntu.com/a/1104520/558486
(run `nm-connection-editor` -> select the ethernet network -> click the cog symbol ->on ipv4 settings tab choose 'share with others').

## set the fs
```
sudo debootstrap --verbose --arch arm64 --variant minbase testing arm64_rootfs http://deb.debian.org/debian/
```
We can also chroot the fs from laptop (I don't think it is necessary).
This is done by:
```
sudo apt install qemu-user-static
sudo cp /usr/bin/qemu-aarch64-static ~/git/rockchip3399/arm64_rootfs/usr/bin/
sudo chroot ~/git/rockchip3399/arm64_rootfs/
```

## set tftp
to lauch the tftp server:
```
sudo apt install tftpd-hpa
```
and make sure it runs:
```
sudo systemctl status tftpd-hpa
● tftpd-hpa.service - LSB: HPA's tftp server
     Loaded: loaded (/etc/init.d/tftpd-hpa; generated)
     Active: active (running) since Sat 2022-09-17 06:49:17 IDT; 10min ago
       Docs: man:systemd-sysv-generator(8)
      Tasks: 1 (limit: 57333)
     Memory: 22.6M
     CGroup: /system.slice/tftpd-hpa.service
             └─48484 /usr/sbin/in.tftpd --listen --user tftp --address :69 --secure /srv/tftp

Sep 17 06:49:17 guri systemd[1]: Starting LSB: HPA's tftp server...
Sep 17 06:49:17 guri tftpd-hpa[48476]:  * Starting HPA's tftpd in.tftpd
Sep 17 06:49:17 guri tftpd-hpa[48476]:    ...done.
Sep 17 06:49:17 guri systemd[1]: Started LSB: HPA's tftp server.
```
We can see it is configured to listen on directory /srv/tftp by default this is the directory
that the tftp clinet (the rock-pi4) will look for files in.
Copy the files we need (kernel image + dtb)

```
sudo mkdir -p /srv/nfs/debian/testing /srv/tftp/boot
sudo mkdir -yyp /srv/tftp/pxelinux.cfg
sudo vim /srv/tftp/pxelinux.cfg/default-arm-rockchip
```
and copy the text: (replace 'YOUR_IP' with your local ip)
```
default l0
menu title U-Boot menu
prompt 0
timeout 50

label l0
        menu label Rock Pi 4 (Debian/Testing)
        linux /boot/Image
        fdt /boot/fdtdir/rockchip/rk3399-rock-pi-4b.dtb
        append loglevel=8 root=/dev/nfs ip=dhcp rootwait rw earlyprintk nfsroot=YOUR_IP:/srv/nfs/debian/testing,v3
```
Note that there are now 3 version for the dtb file (a/b/c), in this tutorial with use the 'b' version.

## move rootfs to /srv
Now we need to move the rootfs to the srv so:

```
sudo cp -aR ~/git/rockchip3399/arm64_rootfs/* /srv/nfs/debian/testing/
```

## Kernel/FDT/Modules for NFS boot

Make sure you install the Kernel/FDT/Modules for NFS boot:

```
dafna@guri:~/git/build/rockchip$ sudo cp arch/arm64/boot/Image /srv/tftp/boot/
dafna@guri:~/git/build/rockchip$ sudo make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" INSTALL_DTBS_PATH=/srv/tftp/boot/fdtdir/ dtbs_install
afna@guri:~/git/build/rockchip$ sudo -E make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" INSTALL_MOD_PATH=/srv/nfs/debian/testing/ modules_install
```

Now connect with [uart](800px-Serial-connection.jpeg):
```
sudo apt install picocom
picocom -b 1500000 /dev/ttyUSB0
```

on my uboot I have:
```
=> print
arch=arm
baudrate=1500000
board=evb_rk3399
board_name=evb_rk3399
boot_a_script=load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}
boot_efi_binary=if fdt addr ${fdt_addr_r}; then bootefi bootmgr ${fdt_addr_r};else bootefi bootmgr ${fdtcontroladdr};fi;load ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r} efi/boot/bootaa64.efi; if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r};else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi
boot_extlinux=sysboot ${devtype} ${devnum}:${distro_bootpart} any ${scriptaddr} ${prefix}${boot_syslinux_conf}
boot_net_usb_start=usb start
boot_prefixes=/ /boot/
boot_script_dhcp=boot.scr.uimg
boot_scripts=boot.scr.uimg boot.scr
boot_syslinux_conf=extlinux/extlinux.conf
boot_targets=mmc0 mmc1 usb0 pxe dhcp
bootargs=console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3 nfsrootdebug loglevel=8 no_console_suspend memblock=debug
bootcmd=run mytftk
bootcmd_dhcp=run boot_net_usb_start; if dhcp ${scriptaddr} ${boot_script_dhcp}; then source ${scriptaddr}; fi;setenv efi_fdtfile ${fdtfile}; setenv efi_old_vci ${bootp_vci};setenv efi_old_arch ${bootp_arch};setenv bootp_vci PXEClient:Arch:00011:UNDI:003000;setenv bootp_arch 0xb;if dhcp ${kernel_addr_r}; then tftpboot ${fdt_addr_r} dtb/${efi_fdtfile};if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r}; else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi;fi;setenv bootp_vci ${efi_old_vci};setenv bootp_arch ${efi_old_arch};setenv efi_fdtfile;setenv efi_old_arch;setenv efi_old_vci;
bootcmd_mmc0=devnum=0; run mmc_boot
bootcmd_mmc1=devnum=1; run mmc_boot
bootcmd_pxe=run boot_net_usb_start; dhcp; if pxe get; then pxe boot; fi
bootcmd_usb0=devnum=0; run usb_boot
bootdelay=2
cpu=armv8
cpuid#=b06ef27d000000003412f57f00000000
distro_bootcmd=for target in ${boot_targets}; do run bootcmd_${target}; done
efi_dtb_prefixes=/ /dtb/ /dtb/current/
ethact=ethernet@fe300000
ethaddr=ce:91:15:cc:76:ef
fdt_addr_r=0x01f00000
fdtcontroladdr=7df26ef8
fdtfile=rockchip/rk3399-rock-pi-4b.dtb
ipaddr=10.42.0.2
jytftk=tftpboot 0x02000000 boot/Image; tftpboot 0x01f00000 boot/fdtdir/rockchip/rk3399-rock-pi-4b.dtb; booti 0x02000000 - 0x01f00000
jytftp=tftpboot 0x02000000 boot/Image; tftpboot 0x01f00000 boot/fdtdir/rockchip/rk3399-rock-pi-4b.dtb; booti 0x02000000 - 0x01f00000
kernel_addr_r=0x02080000
load_efi_dtb=load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}
mmc_boot=if mmc dev ${devnum}; then devtype=mmc; run scan_dev_for_boot_part; fi
mytftk=tftpboot 0x02000000 boot/Image; tftpboot 0x01f00000 boot/fdtdir/rockchip/rk3399-rock-pi-4b.dtb; booti 0x02000000 - 0x01f00000
mytftp=tftpboot 0x02000000 boot/Image; tftpboot 0x01f00000 boot/fdtdir/rockchip/rk3399-rock-pi-4.dtb; booti 0x02000000 - 0x01f00000
partitions=uuid_disk=${uuid_gpt_disk};name=loader1,start=32K,size=4000K,uuid=${uuid_gpt_loader1};name=loader2,start=8MB,size=4MB,uuid=${uuid_gpt_loader2};name=trust,size=4M,uuid=${uuid_gpt_atf};name=boot,size=112M,bootable,uuid=${uuid_gpt_boot};name=rootfs,size=-,uuid=B921B045-1DF0-41C3-AF44-4C6F280D3FAE;
pxefile_addr_r=0x00600000
ramdisk_addr_r=0x06000000
scan_dev_for_boot=echo Scanning ${devtype} ${devnum}:${distro_bootpart}...; for prefix in ${boot_prefixes}; do run scan_dev_for_extlinux; run scan_dev_for_scripts; done;run scan_dev_for_efi;
scan_dev_for_boot_part=part list ${devtype} ${devnum} -bootable devplist; env exists devplist || setenv devplist 1; for distro_bootpart in ${devplist}; do if fstype ${devtype} ${devnum}:${distro_bootpart} bootfstype; then run scan_dev_for_boot; fi; done; setenv devplist
scan_dev_for_efi=setenv efi_fdtfile ${fdtfile}; for prefix in ${efi_dtb_prefixes}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${efi_fdtfile}; then run load_efi_dtb; fi;done;if test -e ${devtype} ${devnum}:${distro_bootpart} efi/boot/bootaa64.efi; then echo Found EFI removable media binary efi/boot/bootaa64.efi; run boot_efi_binary; echo EFI LOAD FAILED: continuing...; fi; setenv efi_fdtfile
scan_dev_for_extlinux=if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${boot_syslinux_conf}; then echo Found ${prefix}${boot_syslinux_conf}; run boot_extlinux; echo SCRIPT FAILED: continuing...; fi
scan_dev_for_scripts=for script in ${boot_scripts}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${script}; then echo Found U-Boot script ${prefix}${script}; run boot_a_script; echo SCRIPT FAILED: continuing...; fi; done
scriptaddr=0x00500000
serial#=87392540ed63b31b
serverip=10.42.0.1
soc=rk3399
stderr=serial@ff1a0000
stdin=serial@ff1a0000
stdout=serial@ff1a0000
usb_boot=usb start; if usb dev ${devnum}; then devtype=usb; run scan_dev_for_boot_part; fi
vendor=rockchip

Environment size: 4804/32764 bytes
=> ping 10.42.0.1
Speed: 1000, full duplex
Using ethernet@fe300000 device
host 10.42.0.1 is alive
=> run mytftk
```
and I get: http://ix.io/4aHR

It seems that the flood of memblock debug prints come from the argument 'memblock=debug' in the 
uboot config 'bootargs'. So let's remove it:

```
=> print bootargs
bootargs=console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3 nfsrootdebug loglevel=8 no_console_suspend memblock=debug
=> setenv bootargs console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3 nfsrootdebug loglevel=8 no_console_suspend
=> setenv bootargs console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3                                                                                                                                           print bootargs
bootargs=console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3 nfsrootdebug loglevel=8 no_console_suspend
=> saveenv
Saving Environment to MMC... Writing to MMC(0)... OK
```

I get http://ix.io/4aI1

I see now:
```
    0.000000] Kernel command line: console=ttyS2,1500000n8 ip=dhcp root=/dev/nfs rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,tcp,v3 nfsrootdebug loglevel=8 no_console_suspend
[    0.000000] Unknown kernel command line parameters "earlyprintk nfsrootdebug", will be passed to user space.
```
So first I notice I didn't copy the rootfs I created to /srv, and second , the kernel didn't recognize nfsrootdebug.
I see that nfsrootdebug is relevant only when config 'NFS_DEBUG' is set. We can later recompile the kernel with
`CONFIG_NFS_DEBUG` to solve that.

update: this is solved with
```
sudo cp -aR ~/git/rockchip3399/arm64_rootfs/* /srv/nfs/debian/testing/
```
which I already updated in the tutorial.
Now I face another issue, that there is no init process installed.
This is solved by chrooting to the rootfs and 'apt install systemd-sysv'
Now, this creates the init in `/usr/sbin/init` so I have to setenv in the uboot: `init=/usr/sbin/init`
I now get `

and I still get:
```
[  101.347975] VFS: Unable to mount root fs via NFS.
[  101.348522] devtmpfs: mounted
[  101.350799] Freeing unused kernel memory: 2752K
[  101.361676] Run /usr/sbin/init as init process
[  101.362113]   with arguments:
[  101.362385]     /usr/sbin/init
[  101.362664]   with environment:
[  101.362949]     HOME=/
[  101.363169]     TERM=linux
[  101.363700] Kernel panic - not syncing: Requested init /usr/sbin/init failed (error -2).
[  101.364425] CPU: 1 PID: 1 Comm: swapper/0 Not tainted 6.0.0-rc5-00015-ge839a756012b #2
[  101.365141] Hardware name: Radxa ROCK Pi 4B (DT)
[  101.365560] Call trace:
[  101.365786]  dump_backtrace+0xd4/0xe0
[  101.366147]  show_stack+0x14/0x48
[  101.366467]  dump_stack_lvl+0x64/0x7c
[  101.366819]  dump_stack+0x14/0x2c
[  101.367135]  panic+0x168/0x328
[  101.367426]  kernel_init+0xd4/0x128
[  101.367756]  ret_from_fork+0x10/0x20
[  101.368097] SMP: stopping secondary CPUs
[  101.368611] Kernel Offset: disabled
[  101.368927] CPU features: 0x4000,0820f021,00001086
[  101.369363] Memory Limit: none
[  101.369650] ---[ end Kernel panic - not syncing: Requested init /usr/sbin/init failed (error -2). ]---
```

I think it is realted to the fact that I don't have a user in the rootfs.
When crooting to it the console is:
```
I have no name!@guri:~#
```

But also it is because I didn't have nfs server: `sudo apt install nfs-kernel-server`
Then I have to configure the exported directories for the nfs. so I added the line `/srv     *(ro,sync,subtree_check)`
to /etc/exports:

***(UPDATE: The following configuration in /etc/exports is WRONG! it sets to 'ro' which caused
tons of systemd errors on boot and no login prompt , see further in the tutorial for the
correct configuration)***
```
dafna@guri:/srv/nfs/debian/testing$ cat /etc/exports
# /etc/exports: the access control list for filesystems which may be exported
#		to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
/srv     *(ro,sync,subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#
```
and then run:
```
sudo exportfs -a
sudo systemctl restart nfs-kernel-server
```
and finaly I get systemd messages on boot:
```
Welcome to Debian GNU/Linux bookworm/sid!

[    5.858442] systemd[1]: Hostname set to <guri>.
[    6.656878] systemd[1]: Queued start job for default target Graphical Interface.
[    6.677412] systemd[1]: Created slice Slice /system/getty.
[  OK  ] Created slice Slice /system/getty.
[    6.681474] systemd[1]: Created slice Slice /system/modprobe.
[  OK  ] Created slice Slice /system/modprobe.
[    6.685004] systemd[1]: Created slice Slice /system/serial-getty.
[  OK  ] Created slice Slice /system/serial-getty.
[    6.688150] systemd[1]: Created slice User and Session Slice.
[  OK  ] Created slice User and Session Slice.
[    6.698972] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Started Dispatch Password …ts to Console Directory Watch.
[    6.711298] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password R…uests to Wall Directory Watch.
[    6.723124] systemd[1]: Arbitrary Executable File Formats File System Automount Point was skipped because of a failed condition check (ConditionPathExists=/proc/sys/fs/binfmt_misc).
[    6.726369] systemd[1]: Reached target Local Encrypted Volumes.
[  OK  ] Reached target Local Encrypted Volumes.
[    6.728286] systemd[1]: Reached target Local Integrity Protected Volumes.
[  OK  ] Reached target Local Integrity Protected Volumes.
[    6.738866] systemd[1]: Reached target Path Units.
[  OK  ] Reached target Path Units.
[    6.751101] systemd[1]: Reached target Remote File Systems.
[  OK  ] Reached target Remote File Systems.
[    6.753004] systemd[1]: Reached target Slice Units.
[  OK  ] Reached target Slice Units.
[    6.763137] systemd[1]: Reached target Swaps.
[  OK  ] Reached target Swaps.
[    6.775216] systemd[1]: Reached target Local Verity Protected Volumes.
[  OK  ] Reached target Local Verity Protected Volumes.
[    6.778098] systemd[1]: Listening on initctl Compatibility Named Pipe.
[  OK  ] Listening on initctl Compatibility Named Pipe.
[    6.783027] systemd[1]: Listening on Journal Audit Socket.
[  OK  ] Listening on Journal Audit Socket.
[    6.787321] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[    6.800034] systemd[1]: Listening on Journal Socket.
[  OK  ] Listening on Journal Socket.
[    6.847062] systemd[1]: Mounting Huge Pages File System...
         Mounting Huge Pages File System...
[    6.863686] systemd[1]: Mounting POSIX Message Queue File System...
         Mounting POSIX Message Queue File System...
[    6.871763] systemd[1]: Mounting Kernel Debug File System...
         Mounting Kernel Debug File System...
[    6.883365] systemd[1]: Kernel Trace File System was skipped because of a failed condition check (ConditionPathExists=/sys/kernel/tracing).
[    6.892659] systemd[1]: Starting Create List of Static Device Nodes...
         Starting Create List of Static Device Nodes...
[    6.902720] systemd[1]: Starting Load Kernel Module configfs...
         Starting Load Kernel Module configfs...
[    6.923386] systemd[1]: Starting Load Kernel Module drm...
         Starting Load Kernel Module drm...
[    6.943900] systemd[1]: Starting Load Kernel Module fuse...
         Starting Load Kernel Module fuse...
[    6.966026] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[    7.019663] systemd[1]: Starting Load Kernel Modules...
         Starting Load Kernel Modules...
[    7.037077] systemd[1]: Starting Remount Root and Kernel File Systems...
         Starting Remount Root and Kernel File Systems...
[    7.041223] systemd[1]: Repartition Root Disk was skipped because all trigger condition checks failed.
[    7.048277] systemd[1]: Mounted Huge Pages File System.
[  OK  ] Mounted Huge Pages File System.
[    7.050511] systemd[1]: Mounted POSIX Message Queue File System.
[  OK  ] Mounted POSIX Message Queue File System.
[    7.052693] systemd[1]: Mounted Kernel Debug File System.
[  OK  ] Mounted Kernel Debug File System.
[    7.054672] systemd[1]: kmod-static-nodes.service: Main process exited, code=exited, status=203/EXEC
[    7.056213] systemd[1]: kmod-static-nodes.service: Failed with result 'exit-code'.
[    7.057710] systemd[1]: Failed to start Create List of Static Device Nodes.
[FAILED] Failed to start Create List of Static Device Nodes.
See 'systemctl status kmod-static-nodes.service' for details.
[    7.084248] systemd[1]: modprobe@configfs.service: Deactivated successfully.
[    7.085571] systemd[1]: Finished Load Kernel Module configfs.
[  OK  ] Finished Load Kernel Module configfs.
[    7.088355] systemd[1]: modprobe@drm.service: Deactivated successfully.
[    7.089633] systemd[1]: Finished Load Kernel Module drm.
[  OK  ] Finished Load Kernel Module drm.
[    7.092478] systemd[1]: modprobe@fuse.service: Deactivated successfully.
[    7.093668] systemd[1]: Finished Load Kernel Module fuse.
[  OK  ] Finished Load Kernel Module fuse.
[    7.097576] systemd[1]: Finished Load Kernel Modules.
[  OK  ] Finished Load Kernel Modules.
[    7.112381] systemd[1]: Finished Remount Root and Kernel File Systems.
[  OK  ] Finished Remount Root and Kernel File Systems.
[    7.123713] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
         Mounting Kernel Configuration File System...
         Starting Flush Journal to Persistent Storage...
         Starting Load/Save Random Seed...
         Starting Apply Kernel Variables...
         Starting Create System Users...
[    7.224021] systemd-journald[177]: Received client request to flush runtime journal.
[  OK  ] Mounted Kernel Configuration File System.
[  OK  ] Finished Flush Journal to Persistent Storage.
[  OK  ] Finished Load/Save Random Seed.
[  OK  ] Finished Apply Kernel Variables.
[FAILED] Failed to start Create System Users.
See 'systemctl status systemd-sysusers.service' for details.
         Starting Create Static Device Nodes in /dev...
[  OK  ] Finished Create Static Device Nodes in /dev.
[  OK  ] Reached target Preparation for Local File Systems.
[  OK  ] Reached target Local File Systems.
         Starting Create Volatile Files and Directories...
[  OK  ] Finished Create Volatile Files and Directories.
         Starting Record System Boot/Shutdown in UTMP...
[  OK  ] Finished Record System Boot/Shutdown in UTMP.
[  OK  ] Reached target System Initialization.
[  OK  ] Started Daily dpkg database backup timer.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Reached target Timer Units.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Listening on UUID daemon activation socket.
[  OK  ] Reached target Socket Units.
[  OK  ] Reached target Basic System.
         Starting D-Bus System Message Bus...
         Starting User Login Management...
         Starting Permit User Sessions...
[    8.110459] random: dbus-daemon: uninitialized urandom read (12 bytes read)
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Permit User Sessions.
[  OK  ] Finished Load Kernel Module drm.
[  OK  ] Started Getty on tty1.
[    8.162144] random: dbus-daemon: uninitialized urandom read (12 bytes read)
         Starting User Login Management...
[FAILED] Failed to start D-Bus System Message Bus.
See 'systemctl status dbus.service' for details.
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
         Starting User Login Management...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
         Starting User Login Management...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
         Starting User Login Management...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[***   ] A start job is run[   49.357475] random: crng init done
[ TIME ] Timed out waiting for device /dev/ttyS2.
[DEPEND] Dependency failed for Serial Getty on ttyS2.
[  OK  ] Reached target Login Prompts.
[  OK  ] Reached target Multi-User System.
[  OK  ] Reached target Graphical Interface.
         Starting Record Runlevel Change in UTMP...
[  OK  ] Finished Record Runlevel Change in UTMP.
```

Now I think I really have to make the /root user on the rootfs.

I copied to 'root' entries in the files /etc/passwd /etc/shadow in my ubuntu and copied them
to the same files in the rootfs. Then I did `passwd root` (password is `aaaaa`) and I became root.
then I added the `_apt` user with: `sudo adduser _apt --force-badname`

Then I installed essentials:
```
root@guri:/etc# apt install -y apt-utils
root@guri:/etc# apt install -y udev kmod net-tools iproute2 libudev-dev
```
I get some warnings when installing. Let's see if it is curacial

I now get to the login page, still with lot's of systemd erros, and I also not able to login
(wrong password):
```
Welcome to Debian GNU/Linux bookworm/sid!

[    5.975332] systemd[1]: Hostname set to <guri>.
[    6.956086] systemd[1]: Queued start job for default target Graphical Interface.
[    6.990357] systemd[1]: Created slice Slice /system/getty.
[  OK  ] Created slice Slice /system/getty.
[    6.994531] systemd[1]: Created slice Slice /system/modprobe.
[  OK  ] Created slice Slice /system/modprobe.
[    6.998198] systemd[1]: Created slice Slice /system/serial-getty.
[  OK  ] Created slice Slice /system/serial-getty.
[    7.001289] systemd[1]: Created slice User and Session Slice.
[  OK  ] Created slice User and Session Slice.
[    7.003787] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Started Dispatch Password …ts to Console Directory Watch.
[    7.014692] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password R…uests to Wall Directory Watch.
[    7.026520] systemd[1]: Arbitrary Executable File Formats File System Automount Point was skipped because of a failed condition check (ConditionPathExists=/proc/sys/fs/binfmt_misc).
[    7.029756] systemd[1]: Reached target Local Encrypted Volumes.
[  OK  ] Reached target Local Encrypted Volumes.
[    7.031982] systemd[1]: Reached target Local Integrity Protected Volumes.
[  OK  ] Reached target Local Integrity Protected Volumes.
[    7.042581] systemd[1]: Reached target Path Units.
[  OK  ] Reached target Path Units.
[    7.054896] systemd[1]: Reached target Remote File Systems.
[  OK  ] Reached target Remote File Systems.
[    7.056842] systemd[1]: Reached target Slice Units.
[  OK  ] Reached target Slice Units.
[    7.066829] systemd[1]: Reached target Swaps.
[  OK  ] Reached target Swaps.
[    7.078935] systemd[1]: Reached target Local Verity Protected Volumes.
[  OK  ] Reached target Local Verity Protected Volumes.
[    7.081850] systemd[1]: Listening on initctl Compatibility Named Pipe.
[  OK  ] Listening on initctl Compatibility Named Pipe.
[    7.086796] systemd[1]: Listening on Journal Audit Socket.
[  OK  ] Listening on Journal Audit Socket.
[    7.091016] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[    7.103774] systemd[1]: Listening on Journal Socket.
[  OK  ] Listening on Journal Socket.
[    7.120160] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[    7.123251] systemd[1]: Listening on udev Kernel Socket.
[  OK  ] Listening on udev Kernel Socket.
[    7.158721] systemd[1]: Mounting Huge Pages File System...
         Mounting Huge Pages File System...
[    7.175330] systemd[1]: Mounting POSIX Message Queue File System...
         Mounting POSIX Message Queue File System...
[    7.183529] systemd[1]: Mounting Kernel Debug File System...
         Mounting Kernel Debug File System...
[    7.194877] systemd[1]: Kernel Trace File System was skipped because of a failed condition check (ConditionPathExists=/sys/kernel/tracing).
[    7.203374] systemd[1]: Starting Create List of Static Device Nodes...
         Starting Create List of Static Device Nodes...
[    7.213605] systemd[1]: Starting Load Kernel Module configfs...
         Starting Load Kernel Module configfs...
[    7.230641] systemd[1]: Starting Load Kernel Module drm...
         Starting Load Kernel Module drm...
[    7.247536] systemd[1]: Starting Load Kernel Module fuse...
         Starting Load Kernel Module fuse...
[    7.301210] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[    7.328468] fuse: init (API version 7.36)
[    7.341911] systemd[1]: Starting Load Kernel Modules...
         Starting Load Kernel Modules...
[    7.359833] systemd[1]: Starting Remount Root and Kernel File Systems...
         Starting Remount Root and Kernel File Systems...
[    7.373843] systemd[1]: Repartition Root Disk was skipped because all trigger condition checks failed.
[    7.378703] systemd[1]: Starting Coldplug All udev Devices...
         Starting Coldplug All udev Devices...
[    7.395055] systemd[1]: Mounted Huge Pages File System.
[  OK  ] Mounted Huge Pages File System.
[    7.397157] systemd[1]: Mounted POSIX Message Queue File System.
[  OK  ] Mounted POSIX Message Queue File System.
[    7.399628] systemd[1]: Mounted Kernel Debug File System.
[  OK  ] Mounted Kernel Debug File System.
[    7.402447] systemd[1]: Finished Create List of Static Device Nodes.
[  OK  ] Finished Create List of Static Device Nodes.
[    7.417166] systemd[1]: modprobe@configfs.service: Deactivated successfully.
[    7.418981] systemd[1]: Finished Load Kernel Module configfs.
[  OK  ] Finished Load Kernel Module configfs.
[    7.431632] systemd[1]: modprobe@drm.service: Deactivated successfully.
[    7.432990] systemd[1]: Finished Load Kernel Module drm.
[  OK  ] Finished Load Kernel Module drm.
[    7.435895] systemd[1]: modprobe@fuse.service: Deactivated successfully.
[    7.437253] systemd[1]: Finished Load Kernel Module fuse.
[  OK  ] Finished Load Kernel Module fuse.
[    7.439838] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
[  OK  ] Finished Load Kernel Modules.
[  OK  ] Finished Remount Root and Kernel File Systems.
         Mounting FUSE Control File System...
         Mounting Kernel Configuration File System...
         Starting Flush Journal to Persistent Storage...
         Starting Load/Save Random Seed...
         Starting Apply Kernel Variables...
         Starting Create System Users...
[  OK  ] Mounted FUSE Control File System.
[    7.606865] systemd-journald[177]: Received client request to flush runtime journal.
[  OK  ] Mounted Kernel Configuration File System.
[  OK  ] Finished Flush Journal to Persistent Storage.
[  OK  ] Finished Load/Save Random Seed.
[  OK  ] Finished Apply Kernel Variables.
[FAILED] Failed to start Create System Users.
See 'systemctl status systemd-sysusers.service' for details.
         Starting Create Static Device Nodes in /dev...
[  OK  ] Finished Coldplug All udev Devices.
[  OK  ] Finished Create Static Device Nodes in /dev.
[  OK  ] Reached target Preparation for Local File Systems.
[  OK  ] Reached target Local File Systems.
         Starting Create Volatile Files and Directories...
         Starting Rule-based Manage…for Device Events and Files...
[  OK  ] Started Rule-based Manager for Device Events and Files.
[  OK  ] Finished Create Volatile Files and Directories.
         Starting Network Time Synchronization...
         Starting Record System Boot/Shutdown in UTMP...
[FAILED] Failed to start Network Time Synchronization.
See 'systemctl status systemd-timesyncd.service' for details.
[  OK  ] Found device /dev/ttyS2.
[  OK  ] Finished Record System Boot/Shutdown in UTMP.
[  OK  ] Stopped Network Time Synchronization.
         Starting Network Time Synchronization...
[FAILED] Failed to start Network Time Synchronization.
[    8.451580] rk808-rtc rk808-rtc: registered as rtc0
[    8.452673] rk808-rtc rk808-rtc: setting system clock to 2013-01-18T08:50:30 UTC (1358499030)
See 'systemctl status systemd-timesyncd.service' for details.
[    8.456566] systemd-journald[177]: Time jumped backwards, rotating.
[    8.473577] rockchip-pcie f8000000.pcie: host bridge /pcie@f8000000 ranges:
[    8.474356] rockchip-pcie f8000000.pcie:      MEM 0x00fa000000..0x00fbdfffff -> 0x00fa000000
[    8.475103] rockchip-pcie f8000000.pcie:       IO 0x00fbe00000..0x00fbefffff -> 0x00fbe00000
[    8.476230] rockchip-pcie f8000000.pcie: no vpcie12v regulator found
[    8.505684] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[  OK  ] Stopped Network Time Synchronization.
[    8.548180] rockchip-rga ff680000.rga: HW Version: 0x03.02
[    8.549080] rockchip-rga ff680000.rga: Registered rockchip-rga as /dev/video0
         Starting Network Time Synchronization...
[FAILED] Failed to start Network Time Synchronization.
See 'systemctl status systemd-timesyncd.service' for details.
[  OK  ] Stopped Network Time Synchronization.
[    8.609400] panfrost ff9a0000.gpu: clock rate = 500000000
[    8.613826] Bluetooth: Core ver 2.22
[    8.614485] NET: Registered PF_BLUETOOTH protocol family
[    8.614955] Bluetooth: HCI device and connection manager initialized
[    8.616597] Bluetooth: HCI socket layer initialized
[    8.616940] panfrost ff9a0000.gpu: [drm:panfrost_devfreq_init [panfrost]] Failed to register cooling device
[    8.617038] Bluetooth: L2CAP socket layer initialized
[    8.618030] panfrost ff9a0000.gpu: mali-t860 id 0x860 major 0x2 minor 0x0 status 0x0
[    8.618977] Bluetooth: SCO socket layer initialized
[    8.618990] panfrost ff9a0000.gpu: features: 00000000,00000407, issues: 00000000,24040400
[    8.620250] panfrost ff9a0000.gpu: Features: L2:0x07120206 Shader:0x00000000 Tiler:0x00000809 Mem:0x1 MMU:0x00002830 AS:0xff JS:0x7
[    8.621290] panfrost ff9a0000.gpu: shader_present=0xf l2_present=0x1
[    8.623488] [drm] Initialized panfrost 1.2.0 20180908 for ff9a0000.gpu on minor 0
         Starting Network Time Synchronization...
[    8.636183] hantro_vpu: module is from the staging directory, the quality is unknown, you have been warned.
[    8.639273] hantro-vpu ff650000.video-codec: Adding to iommu group 0
[    8.640425] hantro-vpu ff650000.video-codec: registered rockchip,rk3399-vpu-enc as /dev/video1
[    8.641801] hantro-vpu ff650000.video-codec: registered rockchip,rk3399-vpu-dec as /dev/video2
[    8.643484] rockchip-vop ff8f0000.vop: Adding to iommu group 2
[FAILED] Failed to start Network Time Synchronization.
[    8.649822] cfg80211: Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[    8.651403] platform regulatory.0: Direct firmware load for regulatory.db failed with error -2
[    8.651460] rockchip-vop ff900000.vop: Adding to iommu group 3
[    8.652260] cfg80211: failed to load regulatory.db
See 'systemctl status systemd-timesyncd.service' for details.
[    8.666405] rockchip-drm display-subsystem: bound ff8f0000.vop (ops vop_component_ops [rockchipdrm])
[    8.667346] [drm] unsupported AFBC format[3231564e]
[    8.669527] rockchip-drm display-subsystem: bound ff900000.vop (ops vop_component_ops [rockchipdrm])
[    8.670608] dwhdmi-rockchip ff940000.hdmi: supply avdd-0v9 not found, using dummy regulator
[    8.672131] dwhdmi-rockchip ff940000.hdmi: supply avdd-1v8 not found, using dummy regulator
[    8.673085] dwhdmi-rockchip ff940000.hdmi: Detected HDMI TX controller v2.11a with HDCP (DWC HDMI 2.0 TX PHY)
[    8.673321] Bluetooth: HCI UART driver ver 2.3
[    8.674373] Bluetooth: HCI UART protocol H4 registered
[    8.674874] Bluetooth: HCI UART protocol LL registered
[    8.675548] Bluetooth: HCI UART protocol Broadcom registered
[    8.675570] rockchip-drm display-subsystem: bound ff940000.hdmi (ops dw_hdmi_rockchip_ops [rockchipdrm])
[    8.676067] Bluetooth: HCI UART protocol QCA registered
[  OK  ] Stopped Network Time Synchronizatio[    8.677838] [drm] Initialized rockchip 1.0.0 20140818 for display-subsystem on minor 1
[    8.678678] Bluetooth: HCI UART protocol Marvell registered
n.
[    8.678990] rockchip-drm display-subsystem: [drm] Cannot find any crtc or sizes
[    8.679951] rockchip-drm display-subsystem: [drm] Cannot find any crtc or sizes
         Starting Network Time Synchronization...
[FAILED] Failed to start Network Time Synchronization.
[    8.711289] brcmfmac: brcmf_fw_alloc_request: using brcm/brcmfmac43456-sdio for chip BCM4345/9
[    8.712164] brcmfmac mmc2:0001:1: Direct firmware load for brcm/brcmfmac43456-sdio.radxa,rockpi4b.bin failed with error -2
[    8.713216] brcmfmac mmc2:0001:1: Direct firmware load for brcm/brcmfmac43456-sdio.bin failed with error -2
See 'systemctl status systemd-timesyncd.service' for details.
[  OK  ] Listening on Load/Save RF …itch Status /dev/rfkill Watch.
[  OK  ] Stopped Network Time Synchronization.
[FAILED] Failed to start Network Time Synchronization.
See 'systemctl status systemd-timesyncd.service' for details.
[  OK  ] Reached target System Initialization.
[    8.794204] dw-apb-uart ff180000.serial: failed to request DMA
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Reached target System Time Set.
[  OK  ] Started Daily apt download activities.
[  OK  ] Started Daily apt upgrade and clean activities.
[  OK  ] Started Daily dpkg database backup timer.
[  OK  ] Reached target Timer Units.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Listening on UUID daemon activation socket.
[  OK  ] Reached target Socket Units.
[  OK  ] Reached target Basic System.
         Starting D-Bus System Message Bus...
[    8.975108] random: dbus-daemon: uninitialized urandom read (12 bytes read)
         Starting User Login Management...
         Starting Permit User Sessions...
[    9.022074] random: dbus-daemon: uninitialized urandom read (12 bytes read)
[FAILED] Failed to start User Login Management.
[    9.034017] rockchip-pcie f8000000.pcie: PCIe link training gen1 timeout!
[    9.035177] Bluetooth: hci0: BCM: chip id 130
[    9.035818] rockchip-pcie: probe of f8000000.pcie failed with error -110
[    9.036428] Bluetooth: hci0: BCM: features 0x0f
[    9.040198] Bluetooth: hci0: BCM4345C5
[    9.040565] Bluetooth: hci0: BCM4345C5 (003.006.006) build 0000
[    9.041463] Bluetooth: hci0: BCM: firmware Patch file not found, tried:
[    9.042237] Bluetooth: hci0: BCM: 'brcm/BCM4345C5.radxa,rockpi4b.hcd'
See 'systemctl status systemd-logind.service' for details.
[    9.042958] Bluetooth: hci0: BCM: 'brcm/BCM4345C5.hcd'
[    9.043910] Bluetooth: hci0: BCM: 'brcm/BCM.radxa,rockpi4b.hcd'
[    9.044446] Bluetooth: hci0: BCM: 'brcm/BCM.hcd'
[  OK  ] Started D-Bus System Message Bus.
[    9.068093] Bluetooth: hci0: Opcode 0x2031 failed: -22
[  OK  ] Finished Permit User Sessions.
[  OK  ] Reached target Bluetooth Support.
[  OK  ] Reached target Sound Card.
[  OK  ] Started Getty on tty1.
[  OK  ] Started Serial Getty on ttyS2.
[  OK  ] Reached target Login Prompts.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
         Starting Load/Save RF Kill Switch Status...
[  OK  ] Finished Load Kernel Module drm.
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
         Starting User Login Management...
         Starting Load/Save RF Kill Switch Status...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
         Starting Load/Save RF Kill Switch Status...
[  OK  ] Finished Load Kernel Module drm.
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
         Starting User Login Management...
         Starting Load/Save RF Kill Switch Status...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
         Starting Load/Save RF Kill Switch Status...
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
[  OK  ] Finished Load Kernel Module drm.
         Starting User Login Management...
[    9.726101] brcmfmac: brcmf_sdio_htclk: HT Avail timeout (1000000): clkctl 0x50
[FAILED] Failed to start Load/Save RF Kill Switch Status.
See 'systemctl status systemd-rfkill.service' for details.
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
         Starting User Login Management...
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Stopped User Login Management.
         Starting Load Kernel Module drm...
[  OK  ] Finished Load Kernel Module drm.
[FAILED] Failed to start User Login Management.
See 'systemctl status systemd-logind.service' for details.
[  OK  ] Reached target Multi-User System.
[  OK  ] Reached target Graphical Interface.
         Starting Record Runlevel Change in UTMP...
[  OK  ] Finished Record Runlevel Change in UTMP.

Debian GNU/Linux bookworm/sid guri ttyS2

guri login: [   19.173111] platform sound: deferred probe pending
[   19.173576] platform sound-dit: deferred probe pending
[   31.490361] random: crng init done

guri login: root
Password:

Login incorrect
guri login: root
Password:

Login incorrect
guri login:
Login timed out after 60 seconds.

Debian GNU/Linux bookworm/sid guri ttyS2

guri login:

```

UPDATE: to save all the nightmehr, I created the rootfs again with:
```
sudo debootstrap --arch arm64 --variant minbase testing arm64_rootfs http://deb.debian.org/debian/
```

This time it did create the root and _apt users, I don't know what went wrong before.

So I change source.list to be:
```
cat arm64_rootfs/etc/apt/sources.list
deb http://deb.debian.org/debian testing main
deb-src http://deb.debian.org/debian testing main
```
Then did:
```
dafna@guri:~/git$ sudo chroot arm64_rootfs apt-get update
dafna@guri:~/git$ sudo chroot arm64_rootfs apt-get install -y git systemd-sysv udev kmod meson build-essential pkg-config net-tools iproute2 libudev-dev
sudo chroot arm64_rootfs passwd root
(aaaa)
```
The remove old rootfs: `dafna@guri:/srv/nfs/debian$ sudo mv testing old-testing` , 'sudo mkdir testing`
and copy the new:
```
sudo cp -aR arm64_rootfs/* /srv/nfs/debian/testing/
```
and not I have to install the modules again:
```
sudo -E make ARCH=arm64 KBUILD_OUTPUT=~/git/build/rockchip CROSS_COMPILE="aarch64-linux-gnu-" INSTALL_MOD_PATH=/srv/nfs/debian/testing/ modules_install
```

***Update: the correct line in /etc/exports should be:***

```
/srv/nfs     *(no_root_squash,rw,sync,no_subtree_check)
```
and after changing it do:
```
sudo exportfs -ar
sudo systemctl restart nfs-kernel-server
```
Current full log: http://ix.io/4aL3

and finally, I found out I didn't install udev, and after installing it I have a login promt.

## dhcp configuration
currently links are not resolve, here is what I tried:
from the chroot:

```
apt install isc-dhcp-client
root@guri:~# cat /etc/network/interfaces
auto eth0
iface eth0 inet dhcp
mkdir /etc/network
cat > /etc/network/interfaces << EOF
auto eth0
iface eth0 inet dhcp
EOF
```
and from device - reboot. This didn't work, so continued from chroot:
```
apt install resolvconf
```
and from device:
```
resolvconf -u
```
also didn't work. Continued from device:
```
apt install systemd-resolved
```
and reboot the device
***UPDATE*** - all those things didn't work. I disabled and masked systemd-resolved service
and then I wrote into /etc/resolv.conf:***
```
nameserver 127.0.0.53
options edns0 trust-ad
nameserver 208.67.222.222
nameserver 208.67.220.220
```
and reboot and now it works, but I think this is like static configuration of
dns and might be overwirtten or something and the right way is to use dhcp somehow.

## installing libcamera
```
mkdir git
cd git
git clone https://git.libcamera.org/libcamera/libcamera.git
cd libcamera
apt install cmake
apt install libyaml-dev python3-yaml python3-ply python3-jinja2
apt install libevent-dev libjpeg-dev
meson build
```

## dts overlays
the cameras imx219, ov5647 are not part of the dts of the device. pincharlt added dts overlay for them.
There are two way to load dts overlays, in the u-boot before kernel is loaded or using a set of
non mainline patches that allow loading overlays dynamicaly using configfs. Let's try the second option.

First, I use pincharlt's kernel: https://gitlab.com/ideasonboard/nxp/linux.git branch pinchartl/v6.0/dev/isp/next
Second, the list of configfs overlay patches are in https://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-drivers.git/log/?h=topic/overlays.
So I did:
```
git remote add overlay git://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-drivers.git
git fetch overlay
# gl is alias for git log --decorate --date=short --pretty=format:"%C(green)%cd - %C(red)%h%Creset - %C(auto)%d%C(reset) %s %C(bold blue)<%an>%Creset"
gl overlay/topic/overlays-v6.0-rc1
git cherry-pick 36be022ea2c5^..6fed30f7962e
```
Then recompile.
I get some warnings:
```
 DTC     arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dtbo
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts:35.3-16: Warning (reg_format): /fragment@1/__overlay__/camera@10:reg: property has invalid length (4 bytes) (#address-cells == 2, #size-cells == 1)
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dtbo: Warning (pci_device_reg): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dtbo: Warning (pci_device_bus_num): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dtbo: Warning (i2c_bus_reg): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dtbo: Warning (spi_bus_reg): Failed prerequisite 'reg_format'
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts:33.20-51.4: Warning (avoid_default_addr_size): /fragment@1/__overlay__/camera@10: Relying on default #address-cells value
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts:33.20-51.4: Warning (avoid_default_addr_size): /fragment@1/__overlay__/camera@10: Relying on default #size-cells value
  DTC     arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dtbo
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dts:26.3-16: Warning (reg_format): /fragment@1/__overlay__/camera@36:reg: property has invalid length (4 bytes) (#address-cells == 2, #size-cells == 1)
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dtbo: Warning (pci_device_reg): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dtbo: Warning (pci_device_bus_num): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dtbo: Warning (i2c_bus_reg): Failed prerequisite 'reg_format'
arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dtbo: Warning (spi_bus_reg): Failed prerequisite 'reg_format'
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dts:24.20-39.4: Warning (avoid_default_addr_size): /fragment@1/__overlay__/camera@36: Relying on default #address-cells value
/home/dafna/git/linux/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-ov5647.dts:24.20-39.4: Warning (avoid_default_addr_size): /fragment@1/__overlay__/camera@36: Relying on default #size-cells value
```

Then do the dtbs+modules install, and copy the kerne Image like before and then copy the dtbo files to the rootfs:
```
dafna@guri:~/git/build$ sudo cp ./rockchip/arch/arm64/boot/dts/rockchip/*.dtbo /srv/nfs/debian/testing/root/dtbo
```
then reboot the device.
in the device:
```
mkdir /config
mount -t configfs none /config
ls /config/
mkdir -p /config/device-tree/overlays/rk3399-rock-pi-4-imx219
cat rk3399-rock-pi-4-imx219.dtbo > /config/device-tree/overlays/rk3399-rock-pi-4-imx219/dtbo
[  679.503711] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /syscon@ff770000/mipi-dphy-rx0/status
[  679.504740] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /isp0@ff910000/status
[  679.505627] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /iommu@ff914000/status
[  679.507174] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/clk_imx219
[  679.508091] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/vcc_imx219
[  679.509001] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/imx219
[  679.511381] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/imx219_out
[  679.512310] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/isp_in
[  679.522872] platform ff910000.isp0: Fixing up cyclic dependency with 4-0010

```


