#include <sys/eventfd.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>             /* Definition of uint64_t */
#include <poll.h>
#include <string.h>

#define handle_error(msg) \
   do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main(int argc, char *argv[])
{
	int rc, efd, j, timeout = 5000; /* 5 sec */
	uint64_t u = 0;
	ssize_t s;
	struct pollfd pfd;
	uint64_t sum = 0;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <num>...\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	efd = eventfd(0, 0);
	if (efd == -1)
		handle_error("eventfd");

	for (j = 1; j < argc; j++) {
		printf("writing %s to efd\n", argv[j]);
		u = strtoull(argv[j], NULL, 0);
		s = write(efd, &u, sizeof(uint64_t));
		if (s != sizeof(uint64_t))
			handle_error("write");
		sum += u;
	}
	printf("completed write loop\n");

	memset(&pfd, 0, sizeof(pfd));
	pfd.events = POLLIN;
	pfd.fd = efd;

	rc = poll(&pfd, 1, timeout);
	if (rc < 0)
		handle_error("poll");
	if (rc == 0) {
		printf("poll time out\n");
		exit(EXIT_FAILURE);
	}
	if (! (pfd.revents & POLLIN)) {
		printf("no pollin\n");
		exit(EXIT_FAILURE);
	}
	printf("rc is %d\n", rc);
	rc = read(efd, &u, sizeof(u));
	printf("u is %lu sum = %lu\n", u, sum);

	return 0;

}

