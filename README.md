# Yet Another Kernel Hands-on Labs repo

A set of small labs containing code
and explanation of various kernel concept.
The labs are build against latest kernel:
git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

Each lab is self contained and compiled into a kernel module.
We use '[virtme](https://git.kernel.org/pub/scm/utils/kernel/virtme/virtme.git)
to run virtual machine (based on qemu) on which we can test the modules.

It is based on a lecture I gave long time ago:
https://www.meetup.com/LE-software-craft-community/events/272788098/

## install and run the VM:
1. create a minimal vm, with console only (no GUI) that uses the host's rootfs,
	this option require some [prepareation](preparation.md).


In this hands-on, modules are build as 'External Modules'
There are instuctions how these shoudl be built in the kernel doc:
https://www.kernel.org/doc/html/latest/kbuild/modules.html


To see your installed modules:
```
ls /lib/modules/`uname -r`/extra
```
We will use the kernel log for debugging. You can open a separate terminal and
run `dmesg -w` to constatntly see the new kernel logs.

You can load the kernel module with:
```
sudo modprobe <module file name without .ko extension>
```
for example:
```
sudo modprobe lab0-minimal-kernel-module
```
and unload it with `-r` option:
```
sudo modprobe -r lab0-minimal-kernel-module
```

if `make modules_install` does not work you can try to load the module with:
```
sudo insmod ./lab0-minimal-kernel-module.ko
```
to see list of lab's modules installed:
```
lsmod | grep lab
```

for labs 3, 5 you will have to create a file node for the device:
```
mknod /dev/lab3 c 455 0
```

An explanation of [Building External Modules](https://www.kernel.org/doc/html/latest/kbuild/modules.html)
