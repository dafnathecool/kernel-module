two questions we will answer:
1. if two threads share an fd to the driver. are their syscalls synced ?
2. if two processes open different fd to the driver, are their syscalls synced?

# userspace.c

This should be called like:

```
sudo ./userspace & ./userspace
```
so that two procesess are accesing the driver each process with it's own fd

# userspace-pthread.c

This should be called:

```
sudo ./userspacep
```

This creates two thread sharing the same fd. 
One thread will delay inside the 'read' syscall for 5 sec.
Meanwhile the other thread will call 'read' syscall and delay half a sec head time

The output is:

```
[ 2835.729463] open device
[ 2835.732063] START READ cpu id 5
[ 2835.732193] delay for 5 sec
[ 2835.732215] START READ cpu id 6
[ 2835.732362] idx is 1 - delay half sec
[ 2836.232989] START READ cpu id 3
[ 2836.233259] idx is 1 - delay half sec
[ 2836.733458] START READ cpu id 3
[ 2836.733650] idx is 1 - delay half sec
[ 2837.234311] START READ cpu id 3
[ 2837.234564] idx is 1 - delay half sec
[ 2837.734914] START READ cpu id 3
[ 2837.735085] idx is 1 - delay half sec
[ 2838.236082] START READ cpu id 3
[ 2838.236372] idx is 1 - delay half sec
[ 2838.736573] START READ cpu id 3
[ 2838.736757] idx is 1 - delay half sec
[ 2839.237726] START READ cpu id 3
[ 2839.237942] idx is 1 - delay half sec
[ 2839.738082] START READ cpu id 3
[ 2839.738357] idx is 1 - delay half sec
[ 2840.238685] START READ cpu id 3
[ 2840.238985] idx is 1 - delay half sec
[ 2840.735686] stop delay for 5 sec
[ 2840.742088] CLOSING device
```

So we see the two threads were inside the 'read' syscall together, so syscalls are NOT synced
