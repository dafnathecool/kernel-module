#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/delay.h>

#include "major-minor.h"


#define MY_DEV_NAME "my-device"

#define SIZE 128

static int idx;

#define DELAY 5000

struct my_driver {
	struct platform_device *pdev;
};

static DEFINE_MUTEX(my_mutex);
static struct my_driver *mdriver;

static int mycdrv_open(struct inode *inode, struct file *file)
{
	pr_debug("open device\n");
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	return 0;
}

static ssize_t mycdrv_write(struct file *file, const char __user *buf, size_t lbuf, loff_t *ppos)
{
	pr_info("START WRITE - clear idx\n");
	idx = 0;
	return 1;
}

static ssize_t mycdrv_read(struct file *file, char __user *buf, size_t lbuf, loff_t *ppos)
{
	int cpu_id = get_cpu();

	pr_info("START READ cpu id %d\n", cpu_id);
	put_cpu();
	if (idx == 0) {
		idx = 1;
		pr_info("delay for %u sec\n", DELAY / 1000);
		mdelay(DELAY);
		pr_info("stop delay for %u sec\n", DELAY / 1000);
		idx = 2;
		return 0;
	}

	pr_info("idx is 1 - delay half sec\n");
	mdelay(500);
	return idx;
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.read = mycdrv_read,
	.write = mycdrv_write,
	.open = mycdrv_open,
	.release = mycdrv_release
};

static unsigned int count = 1;
static struct cdev my_cdev;

static void free_driver(void)
{
	kfree(mdriver);
	mdriver = NULL;
}

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_init(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);
	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	platform_set_drvdata(pdev, mdriver);
	dev_info(&pdev->dev,
		 "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static void my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static void empty(struct device *dev) { }

static struct platform_device my_device = {
	.dev.release = empty,
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;

	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
