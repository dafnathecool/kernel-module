/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include <pthread.h>
#include "major-minor.h"

static const char *mydevpath = "/dev/mydev";
static const char *kfifo_path = "/dev/mykfifo";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("mknod failed %d %d\n", rc, EEXIST);
		return rc;
	}
	return 0;
}

// Function to be executed by the thread
void *print_message(void *arg)
{
	int fd = (int)arg;
	char a = 1;
	int rc;

	do {
		rc = read(fd, &a, sizeof(a));
	} while (rc == 1);
	return NULL;
}

int main(int argc, char **argv)
{
	pthread_t thread;
	int fd, rc;
	char a = 1;

	rc = create_path(mydevpath, my_minor);
	if (rc)
		exit(EXIT_FAILURE);

	fd = open(mydevpath, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}
	if (pthread_create(&thread, NULL, print_message, (void *)fd) != 0) {
		perror("Failed to create thread");
		return 1;
	}

// we have 2 threads. One thread will delay inside the 'read' syscall for 5 sec
// meanwhile the other thread will call 'read' syscall and delay half a sec head time
	do {
		rc = read(fd, &a, sizeof(a));
	} while (rc == 1);

	if (pthread_join(thread, NULL) != 0) {
		perror("Failed to join thread");
		return 1;
	}
	return rc;
}

