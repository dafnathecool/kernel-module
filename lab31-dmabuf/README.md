# DMABUF

The kernel has an API called `dma-buf` which is used to share buffers bewteen
different devices and beteween userspace. A common scenario is for example
piping the frames capured from a camera into the drm subsys to rended the
frames and show them on the dispaly. The dma-buf API allows sharing the
buffers of the frames instead of copying them which save lot's of cpu.

The usage of the API is described here:

https://www.kernel.org/doc/html/latest/driver-api/dma-buf.html
https://lwn.net/Articles/474819/


