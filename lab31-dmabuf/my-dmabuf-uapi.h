/*********************/
/* API for exporters */
/*********************/

/* write 'e' to the driver to export (call dma_buf_export(&exp_info)) */
#define DMABUF_EXP_EXPORT 'e'

/* write 'd' to the driver to return the DMABUF file descriptor */
#define DMABUF_EXP_GET_FD 'd'

/*********************/
/* API for importers */
/*********************/

/* write the stringy of the fd to the driver e.g if the fd is 123 you should write the string '123' */

/* write 'a' to the driver to attach the device */
#define DMABUF_IMP_ATTACH 'a'

/* write 'm' to the driver to call dma_buf_map_attachment */
#define DMABUF_IMP_MAP 'm'

/* start reading from the driver... */

/* write 'u' to the driver to unmap (call dma_buf_unmap_attachment) */
#define DMABUF_IMP_UNMAP 'u'

/* write 'd' to the driver to deattach */
#define DMABUF_IMP_DEATTACH 'd'
