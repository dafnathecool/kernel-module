#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include "my-dmabuf-uapi.h"
#include "dmabuf-uapi.h"

#define MY_DEV_NAME "my-dma-exporter"

#define BUFSIZE 4096

struct dma_exporter {
	struct device *dev;
	char vars_here;
	struct cdev exp_cdev;
}

static inline struct dma_exporter *to_my_dma_exporter(struct *inode)
{
	return container_of(inode->i_cdev, struct dma_exporter, exp_cdev);
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.write = exp_write,
	.open = exp_open,
	.release = exp_release
};

static int exp_open(struct inode *inode, struct file *file)
{
struct :
	file->private_data = mdriver ? mdriver->pdev : NULL;
	mutex_unlock(&my_mutex);
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	file->private_data = NULL;
	return 0;
}

	static ssize_t
mycdrv_read(struct file *file, char __user *buf, size_t lbuf, loff_t *ppos)
{

	struct platform_device *pd = file->private_data;
	size_t name_len;
	ssize_t nbytes;

	if (!pd)
		return -EINVAL;
	name_len = strlen(pd->name);
	if (*ppos >= name_len)
		return 0;

	name_len = name_len - *ppos;

	nbytes = min(lbuf, name_len) - copy_to_user(buf, pd->name + *ppos, min(lbuf, name_len));
	*ppos += nbytes;
	pr_info("READING function, nbytes=%lu, pos=%llu\n", nbytes, *ppos);
	return nbytes;
}

struct my_attach {
	struct sg_table sgt;
	enum dma_data_direction dma_dir;
};

static void my_dma_buff_detach(struct dma_buf *dbuf,
	struct dma_buf_attachment *attachment)
{
	struct my_attach *myattach = attachment->priv;
	struct sg_table *sgt;

	if (!myattach) {
		dev_info(attachment->dev, "%s my attach is null\n", __func__);
		return;
	}
	dev_dbg(attachment->dev, "%s\n", __func__);

	sgt = &myattach->sgt;

	/* release the scatterlist cache */
	if (myattach->dma_dir != DMA_NONE)
		dma_unmap_sgtable(attachment->dev, sgt, myattach->dma_dir, 0);
	sg_free_table(sgt);
	kfree(myattach);
	attachment->priv = NULL;
}

static int my_dma_buff_attach(struct dma_buf *dmabuf, struct dma_buf_attachment *attachment)
{
	struct my_attach *myattach;
	char *vaddr = dbuf->priv;
	int num_pages = PAGE_ALIGN(buf->size) / PAGE_SIZE;
	struct sg_table *sgt;
	struct scatterlist *sg;
	int ret;
	int i;

	dev_dbg(attachment->dev, "%s\n", __func__);
	myattach = kzalloc(sizeof(*attach), GFP_KERNEL);
	if (!myattach)
		return -ENOMEM;

	sgt = &myattach->sgt;
	ret = sg_alloc_table(sgt, num_pages, GFP_KERNEL);
	if (ret) {
		kfree(myattach);
		return ret;
	}
	for_each_sgtable_sg(sgt, sg, i) {
		struct page *page = vmalloc_to_page(vaddr);

		if (!page) {
			sg_free_table(sgt);
			kfree(myattach);
			return -ENOMEM;
		}
		sg_set_page(sg, page, PAGE_SIZE, 0);
		vaddr += PAGE_SIZE;
	}

	myattach->dma_dir = DMA_NONE;
	dbuf_attach->priv = myattach;
	return 0;
}

static struct sg_table *my_dma_buff_map(struct dma_buf_attachment *attachment,
					enum dma_data_direction dma_dir)
{
	struct my_attach *myattach = attachment->priv;
	struct sg_table *sgt;

	sgt = &attach->sgt;
	/* return previously mapped sg table */
	if (attach->dma_dir == dma_dir)
		return sgt;

	/* release any previous cache */
	if (attach->dma_dir != DMA_NONE) {
		dma_unmap_sgtable(attachment->dev, sgt, myattach->dma_dir, 0);
		myattach->dma_dir = DMA_NONE;
	}

	/* mapping to the client with new direction */
	if (dma_map_sgtable(attachment->dev, sgt, dma_dir, 0)) {
		pr_err("failed to map scatterlist\n");
		return ERR_PTR(-EIO);
	}

	myattach->dma_dir = dma_dir;

	return sgt;
}

static const struct dma_buf_ops cma_heap_buf_ops = {
	.attach = my_dma_buff_attach,
	.detach = my_dma_buff_detach,
	.map_dma_buf = my_dma_buff_map,
	.unmap_dma_buf = cma_heap_unmap_dma_buf,
	.begin_cpu_access = cma_heap_dma_buf_begin_cpu_access,
	.end_cpu_access = cma_heap_dma_buf_end_cpu_access,
	.mmap = cma_heap_mmap,
	.vmap = cma_heap_vmap,
	.vunmap = cma_heap_vunmap,
	.release = cma_heap_dma_buf_release,
};

static int exporter_probe(struct platform_device *pdev)
{
	dev_t first = MKDEV(MY_DMABUF_MAJOR, MY_DMABUF_EXP_MINOR);
	struct dma_exporter *exp;
	int ret;
	void *priv_buf;
	DEFINE_DMA_BUF_EXPORT_INFO(exp_info);

	priv_buf = kzalloc(BUFSIZE);
	if (!priv_buf)
		return -ENOMEM;

	exp_info.exp_name = dma_heap_get_name(heap);
	exp_info.ops = &cma_heap_buf_ops;
	exp_info.size = buffer->len;
	exp_info.flags = fd_flags;
	exp_info.priv = priv_buf;

	 = dma_buf_export(&exp_info);

	ret = register_chrdev_region(first, MY_DMABUF_MINORS_NUM, MY_DEV_NAME);
	if (ret) {
		dev_err(&pdev->dev, "failed registering chrdev reg (%d)\n", ret);
		goto err_free_exp;
	}
	cdev_init(&exp->exp_cdev, &mycdrv_fops);

	ret = cdev_add(&exp->exp_cdev, first, 1);
	if (ret) {
		dev_err(pdev->dev, "cdev_add failed (%d)\n", ret);
		goto err_cdev_unreg;
	}
	
	exp->dev = pdev->dev;
	platform_set_drvdata(pdev, exp);
	dev_info(&pdev->dev, "dma exporter registered as major %d minor %d\n",
		 MY_DMABUF_MAJOR, MY_DMABUF_EXP_MINOR);
	return 0;

err_cdev_unreg:
	unregister_chrdev_region(first, MY_DMABUF_MINORS_NUM);
err_free_exp:
	free(exp);
	return ret;
}

static int exporter_remove(struct platform_device *pdev)
{
	char *pd = platform_get_drvdata(pdev);
	dev_dbg(&pdev->dev, "%s: %s!! bye!!\n", __func__, pd);
	kfree(pd);
	return 0;
}

static struct platform_driver exporter_driver = {
	.probe =	exporter_probe,
	.remove =	exporter_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static void dev_release(struct device *dev)
{
}

static struct platform_device exporter_device = {
	.name = MY_DEV_NAME,
	.dev.release = dev_release,
};

static int __init platform_driver_init(void)
{
	int ret;

	pr_debug("%s", __func__);
	ret = platform_device_register(&exporter_device);
	if (ret) {
		pr_err("%s: platform_driver_register failed (%d)\n", __func__, ret);
		return ret;
	}
	ret = platform_driver_register(&exporter_driver);
	if (ret) {
		pr_err("%s: platform_driver_register failed (%d)\n", __func__, ret);
		platform_device_unregister(&exporter_device);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("%s", __func__);
	platform_device_unregister(&exporter_device);
	platform_driver_unregister(&exporter_driver);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_ALIAS("platform:exporter-device");
MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("exporter platform driver");
MODULE_LICENSE("GPL v2");
