#define _GNU_SOURCE
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "my_head.h"


#define SZ_1G				0x40000000

int main(int argc, char *argv[])
{
	int fd = open(argv[1], O_RDWR);
	int volatile *volatile ptr = mmap((void *)MYADDR, MYSIZE + SZ_1G , PROT_READ | PROT_WRITE, MAP_SHARED |
					MAP_FIXED |    MAP_ANONYMOUS, -1, 0);      
	  if (ptr == MAP_FAILED)
                return -1;
	for (int i=0; i<MYSIZE + SZ_1G ; i++)
		ptr[i] = i;
	sleep(10000);
	int *ptr2 = mremap((void *)MYADDR, MYSIZE, MYSIZE, MAP_SHARED | MAP_FIXED |    
                        MAP_ANONYMOUS, MYADDR + 0x1000);
	ptr[0] = ptr[1];
//	munmap(ptr, MYSIZE);
	munmap(ptr2, MYSIZE);
	return 0;
}
		
	
