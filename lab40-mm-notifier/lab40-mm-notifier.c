#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include "my_head.h"

#define MY_DEV_NAME "my-device"

struct platform_device* pdev;

struct mmu_interval_notifier notifier;
unsigned long notifier_seq;

/*
struct mmu_notifier_range {
	struct mm_struct *mm;
	unsigned long start;
	unsigned long end;
	unsigned flags;
	enum mmu_notifier_event event;
	void *owner;
};

   */
static bool my_invalidate_cb(struct mmu_interval_notifier *mni,
			const struct mmu_notifier_range *range,
				unsigned long cur_seq)
{

	pr_err("%s called with mni %p range %p seq %lu:\n", __func__, mni, range, cur_seq);
	pr_err("mm=%p start=%lx end=%lx flags=%x event=%u owner=%p\n\n",
			range->mm,
			range->start,
			range->end,
			range->flags,
			range->event,
			range->owner);
	return true;
}

static const struct mmu_interval_notifier_ops my_notif_ops = {
	.invalidate = my_invalidate_cb,
};

static int mycdrv_open(struct inode *inode, struct file *file)
{
	int err;

	pr_debug("open device\n");
	err = mmu_interval_notifier_insert(&notifier, current->mm, MYADDR, MYSIZE, &my_notif_ops);
	if (err) {
		pr_err("fail to insert notifier %d\n", err);
		return err;
	}
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	file->private_data = NULL;
	return 0;
}

static ssize_t
mycdrv_read(struct file *file, char __user *buf, size_t lbuf, loff_t *ppos)
{
	pr_info("%s called\n", __func__);
	return 0;
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.read = mycdrv_read,
	.open = mycdrv_open,
	.release = mycdrv_release
};

static unsigned int count = 1;
static int my_major = 455, my_minor = 0;
static struct cdev my_cdev;

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);
	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static void empty(struct device *dev)
{
}

static struct platform_device my_device = {
	.dev.release = empty,
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;
	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
