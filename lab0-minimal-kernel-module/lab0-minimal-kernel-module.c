#include <linux/module.h>

#define MY_DEV_NAME "my-device"

static int __init my_init(void)
{
	pr_debug("%s: hurray!\n", __func__);
	return 0;
}

static void __exit my_exit(void)
{
	pr_debug("%s: bye!\n", __func__);
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform device");
MODULE_LICENSE("GPL v2");
