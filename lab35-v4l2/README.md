commit a8fa55078a7784a99a2ce389b5d7456a3be9a941
Author: Janusz Krzysztofik <jmkrzyszt@gmail.com>
Date:   Mon May 20 17:27:45 2019 -0400

    media: v4l2-subdev: Verify arguments in v4l2_subdev_call()

commit a4f4a763d8a01d0f461e3b8c4774ed45e9ded5f4
Author: Janusz Krzysztofik <jmkrzyszt@gmail.com>
Date:   Mon May 20 17:27:46 2019 -0400

    media: v4l2-subdev: Verify v4l2_subdev_call() pointer arguments
    
    Parameters passed to check helpers are now obtained by dereferencing
    unverified pointer arguments.  Check validity of those pointers first.

commit 374d62e7aa50f66c1a4316be9221df4d0f38addd
Author: Janusz Krzysztofik <jmkrzyszt@gmail.com>
Date:   Mon May 20 17:27:47 2019 -0400

    media: v4l2-subdev: Verify v4l2_subdev_call() pad config argument


commit 0d346d2a6f54f06f36b224fd27cd6eafe8c83be9
Author: Tomi Valkeinen <tomi.valkeinen@ideasonboard.com>
Date:   Thu Jun 10 17:55:58 2021 +0300

    media: v4l2-subdev: add subdev-wide state struct

commit 660440a9076b4dcfcc08f56c3cc8c41e3ed6376e
Author: Tomi Valkeinen <tomi.valkeinen@ideasonboard.com>
Date:   Tue Apr 12 10:42:49 2022 +0100

    media: Documentation: add documentation about subdev state
