
branch: https://gitlab.collabora.com/dafna/linux/commits/easycap-v2-coherent-stats-24-jan

ARM64
=====
[gst-main] root@guri:~/gstreamer/build# gst-launch-1.0 v4l2src device=$DEV norm=PAL num-buffers=500 ! fakesink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Redistribute latency...
Got EOS from element "pipeline0".
Execution ended after 0:00:20.087511353
Setting pipeline to NULL ...
Freeing pipeline ...
[gst-main] root@guri:~/gstreamer/build# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
== DMA COHERENT ==
total durations: 20.73551767 sec
urb processing durations: 4.541559160 sec
uS/qty: 4541559/2509 avg: 1810.107 min: 0.583 max: 2113.163 (uS)
FPS: 24.90
lost: 0 done: 500
raw decode speed: 730.738 Mbits/s
bytes 414785444.000 
bytes/urb: 165319

[gst-main] root@guri:~/gstreamer/build# gst-launch-1.0 v4l2src device=$DEV norm=PAL num-buffers=500 ! fakesink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Redistribute latency...
Got EOS from element "pipeline0".
Execution ended after 0:00:20.064120096
Setting pipeline to NULL ...
Freeing pipeline ...
[gst-main] root@guri:~/gstreamer/build# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
== DMA COHERENT ==
total durations: 20.49387668 sec
urb processing durations: 4.511394445 sec
uS/qty: 4511394/2506 avg: 1800.237 min: 0.583 max: 2080.203 (uS)
FPS: 24.93
lost: 0 done: 500
raw decode speed: 735.667 Mbits/s
bytes 414824760.000 
bytes/urb: 165532
[gst-main] root@guri:~/gstreamer/build# 

X86
===
root@(none):/home/dafna/git/vicodec# gst-launch-1.0 v4l2src device=$DEV norm=PAL num-buffers=500 ! video/x-raw,interlace-mode=\(string\)interleaved ! fakesink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:00:20.080958173
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
root@(none):/home/dafna/git/vicodec# cat /sys/kernel/debug/usb/stkvideo/dafna/stats
== DMA COHERENT ==
total durations: 20.220475614 sec
urb processing durations: 0.64751972 sec
uS/qty: 64751/2512 avg: 25.777 min: 0.168 max: 132.250 (uS)
FPS: 24.72
lost: 0 done: 500
raw decode speed: 51.927 Gbits/s
bytes 415422794.000
bytes/urb: 165375

root@(none):/home/dafna/git/vicodec# gst-launch-1.0 v4l2src device=$DEV norm=PAL num-buffers=500 ! video/x-raw,interlace-mode=\(string\)interleaved ! fakesink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:00:20.092949417
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
root@(none):/home/dafna/git/vicodec# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
== DMA COHERENT ==
total durations: 20.220417139 sec
urb processing durations: 0.63069479 sec
uS/qty: 63069/2512 avg: 25.107 min: 0.155 max: 80.996 (uS)
FPS: 24.72
lost: 0 done: 500
raw decode speed: 52.720 Gbits/s
bytes 415173576.000 
bytes/urb: 165276




