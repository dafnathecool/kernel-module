convert the stk1160 to use the new dma api dma_alloc_noncontiguous
=================================================================
I bought an easycap converter.
I conncted it to my television device on my laptop (a Lenovo running Ubuntu)
I was able to capture the video with vlc using the configuration shown in [easycap.png](easycap.png)
I was not able to capture audio with the vlc but was able to capture audio running:
TODO: check possibility of using SDR device

```
arecord -D hw:1,0 -f dat -d 20 a.wav
```
Eze suggested:
```
amixer -c stk1160mixer sset Line unmute cap 
```

Didn't try it yet.

The movement to that new dma api was already done on other drivers:
https://lore.kernel.org/all/CANiDSCsid-gcq_HWWh=M0mOT947F7bj1QYANo4i0TwUSEkkEbg@mail.gmail.com/T/

So it should be something similar to that.
Those statistics came frome this patch:

https://git.kernel.org/pub/scm/linux/kernel/git/kbingham/rcar.git/commit/?h=uvc/async-ml&id=cebbd1b629bbe5f856ec5dc7591478c003f5a944


05 Dec 2021:
============

Trying on Rock-pi4
------------------
in the tutorial https://linuxtv.org/wiki/index.php/Stk1160_based_USB_2.0_video_and_audio_capture_devices
it says that the actuall sound capturing is done by the `snd_usb_audio` module which I had to
enable with CONFIG_SND_USB_AUDIO
Then I was able to record sound on the rock-pi4 with the same command I ran on the laptop:
```
arecord -D hw:1,0 -f dat -d 20 a.wav
```

Trying on the leptop:
---------------------
This eventually worked for me:
```
gst-launch-1.0 v4l2src device=$DEV norm=PAL ! video/x-raw,format=UYVY,width=720,height=480,interlace-mode=\(string\)interleaved ! avimux ! filesink location=x.avi
```
Note the option `interlace-mode=\(string\)interleaved`. Without this option I get:
```
dafna@guri:~/git/gst-build/subprojects$ gst-launch-1.0 v4l2src device=$DEV norm=PAL ! video/x-raw,format=UYVY,width=720,height=480 ! avimux ! filesink location=x
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
ERROR: from element /GstPipeline:pipeline0/GstV4l2Src:v4l2src0: Device '/dev/video2' does not support progressive interlacing
Additional debug info:
gstv4l2object.c(3813): gst_v4l2_object_set_format_full (): /GstPipeline:pipeline0/GstV4l2Src:v4l2src0:
Device wants interleaved interlacing
Execution ended after 0:00:00.000177775
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
```
This capture with color but it had artifacts: [laptop-gst-launch.png](laptop-gst-launch.png)

Then when removing the width and height params for some reason the artifacts disappear:
```
gst-launch-1.0 v4l2src device=$DEV norm=PAL ! video/x-raw,interlace-mode=\(string\)interleaved ! avimux ! filesink location=x2.avi
```
And the exact same command works also on the rock-pi4 !!

21 Dec 2021:
============
So that stats patch from https://git.kernel.org/pub/scm/linux/kernel/git/kbingham/rcar.git
on my easycap branch the commit is:
https://gitlab.collabora.com/dafna/linux/-/commit/693dd66d5f127b5d3e8d422e5c0184031c292e10

But this is for uvc camera, it doesn't have much to do with our easycap,
It could be nice though to see those stats from the UVC camera on the Laptop.
To find the usb host and device number of the uvc cam on the laptop:
```
dafna@guri:~/git/media_tree$ dmesg | grep UVC
[  563.399588] uvcvideo: Found UVC 1.00 device Integrated Camera (13d3:56a6)

dafna@guri:~/git/media_tree$ lsusb | grep "13d3:56a6"
Bus 001 Device 004: ID 13d3:56a6 IMC Networks 
```
So hostbus is 1 and hostaddr is 6
To add those params on virtme, then after `--qemu-opts`
I add `-usb -device usb-ehci,id=ehci -device usb-host,hostbus=1,hostaddr=4`
Also uvc, should be enabled when compiling

So full command is:
```
make modules_install INSTALL_MOD_PATH=~/git/media_tree/tmp
sudo virtme-run --mdir=/home/dafna/git/media_tree/tmp/lib/modules/$KERNELRELEASE/ --cwd /home/dafna/git/vicodec/  --rwdir ../vicodec --kdir $KBUILD_OUTPUT --kopt "console=ttyS0 no_console_suspend" --qemu-opts $MEM_CPU -smp 2 -usb -device usb-ehci,id=ehci -device usb-host,hostbus=1,hostaddr=4
```
then from virtme:
```
v4l2-ctl --stream-mmap  -d /dev/video0
```
and:
```
cat /sys/kernel/debug/usb/uvcvideo/1-2-1/stats
frames:  218
packets: 58477
empty:   12016 (20 %)
errors:  0
invalid: 808
pts: 1 early, 217 initial, 218 ok
scr: 218 count ok, 218 diff ok
sof: 0 <= sof <= 2047, freq 1.001 kHz
bytes 23903940 : duration 7330
FPS: 29.74
URB: 85961/1678 uS/qty: 51.228 avg 30.557 min 440.811 max (uS)
header: 61286/1678 uS/qty: 36.523 avg 0.000 min 393.862 max (uS)
latency: 76616/1678 uS/qty: 45.659 avg 26.807 min 428.357 max (uS)
decode: 9344/1678 uS/qty: 5.569 avg 0.314 min 55.083 max (uS)
raw decode speed: 21.247 Gbits/s
raw URB handling speed: 2.249 Gbits/s
throughput: 26.088 Mbits/s
URB decode CPU usage 0.127400 %
```

Which is the same format reported in commit https://lore.kernel.org/all/CANiDSCsid-gcq_HWWh=M0mOT947F7bj1QYANo4i0TwUSEkkEbg@mail.gmail.com/T/


lets copy it to here is how it is copied to easycap:
https://gitlab.collabora.com/dafna/linux/-/commit/d72aa4a9a7a97261662edef91786f831a9c7b295

and now in the rockchip after streaming we have:
```
[gst-main] root@guri:~/libcamera/build/src/cam# cat  /sys/kernel/debug/usb/stkvideo/dafna/stats 
decode: 220916/128 uS/qty: 1725.912 avg 0.584 min 2162.457 max (uS)
raw decode speed: 721.808 Mbits/s
```
22 Dec 2021:
============
And now to the main task - move to noncontig API.
It should be something similar to this commit: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=20e1dbf2bbe2431072571000ed31dfef09359c08


06 Jan 2022:
============
And now the final: let's test the time with and without the noncontig api.

I Created two branches, one with and one without the patch:

First without the patch:
The branch is:
https://gitlab.collabora.com/dafna/linux/-/commits/easycap-no-noncont
the command is:

```
gst-launch-1.0 v4l2src num-buffers=1000 device=$DEV norm=PAL ! video/x-raw,interlace-mode=\(string\)interleaved ! avimux ! filesink location=/root/jan.avi
```

The output is:
```
[gst-main] root@guri:~/gstreamer# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
coherent: uS/qty: 9581680/5990 avg: 1599.612 min: 0.584 max: 3121.473 (uS)
raw decode speed: 692.664 Mbits/s
```
And several runs with fakesink instead of filesink:
```
coherent: uS/qty: 8839878/5009 avg: 1764.798 min: 0.583 max: 2062.701 (uS)
raw decode speed: 1.501 Gbits/s

coherent: uS/qty: 9071822/5007 avg: 1811.827 min: 0.583 max: 2344.163 (uS)
raw decode speed: 2.194 Gbits/s

coherent: uS/qty: 9038865/5006 avg: 1805.606 min: 0.583 max: 2230.122 (uS)
raw decode speed: 2.936 Gbits/s

coherent: uS/qty: 9035236/5008 avg: 1804.160 min: 0.583 max: 2035.579 (uS)
raw decode speed: 3.672 Gbits/s

```
And now with the contig api:
```
[gst-main] root@guri:~/libcamera/build/src/cam# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
coherent: uS/qty: 572098/5006 sec: 572.98915 avg: 114.282 min: 0.583 max: 211.171 (uS)
raw decode speed: 11.600 Gbits/s
bytes 829459428.000 

```

Jan 9
=====
The audio side:

update: I was also able to stream audio with:
```
gst-launch-1.0 -v alsasrc device=hw:1 ! queue ! audioconvert ! vorbisenc ! oggmux ! filesink location=alsasrc.ogg
```

The audio side is managed by `sound/usb/endpoint.c`
debug prints for that can be enabled with:
```
echo "file sound/usb/* +p" > /sys/kernel/debug/dynamic_debug/control
```

And muxing both audio and video:
```
gst-launch-1.0 v4l2src device=$DEV norm=PAL ! video/x-raw,interlace-mode=\(string\)interleaved ! mux. alsasrc device=hw:1 ! mux. avimux name=mux ! filesink location=/root/a.avi
```
This is from https://www.linuxtv.org/wiki/index.php/GStreamer

Jan 10
======
On x86, run stk1160 from virtme:
```
lsusb | grep -i stk
Bus 001 Device 015: ID 05e1:0408 Syntek Semiconductor Co., Ltd STK1160 Video Capture Device
then add the params to virtme ` -usb -device usb-ehci,id=ehci -device usb-host,hostbus=1,hostaddr=15`
```

doing `ninja -C build devenv` inside virtme didn't work and didn't
have patiant to make it work so I just installed gst-launch from standrd:
```
sudo apt install gstreamer1.0-tools
```

And that mux with audio can't work since we did not exposed qemu to the audio files so
got back to recording video only:
```
gst-launch-1.0 v4l2src device=$DEV norm=PAL ! video/x-raw,interlace-mode=\(string\)interleaved ! avimux ! filesink location=x2.avi
```
And with 10000 buffers:
```
gst-launch-1.0 v4l2src num-buffers=10000 device=$DEV norm=PAL ! video/x-raw,interlace-mode=\(string\)interleaved ! avimux ! filesink location=x2.avi
```

Then with noncontig on x86 on 100000 buffers we have:
```
noncont: uS/qty: 3203891/50096 sec: 3203.891724 avg: 63.955 min: 0.141 max: 11696.636 (uS)
raw decode speed: 23.783 Gbits/s
bytes 8.932543668 G
bytes/urb: 190084
```
Without the noncont we have:
```
coherent: uS/qty: 3081680/50112 sec: 3081.680438 avg: 61.495 min: 0.160 max: 482.099 (uS)
raw decode speed: 21.557 Gbits/s
bytes 7.786260844 G
bytes/urb: 165677
```

And on the rock-pi4 again with the video/audio mux:
```
[gst-main] root@guri:~# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
coherent: uS/qty: 92423219/98904 sec: 92423.219509 avg: 934.474 min: 0.583 max: 2196.583 (uS)
raw decode speed: 787.698 Mbits/s
bytes 8.510250828 G
bytes/urb: 92010

[gst-main] root@guri:~/libcamera/build/src/cam# cat /sys/kernel/debug/usb/stkvideo/dafna/stats 
noncont: uS/qty: 9231963/106048 sec: 9231.963472 avg: 87.054 min: 0.291 max: 216.423 (uS)
raw decode speed: 9.217 Gbits/s
bytes 9.971904900 G
bytes/urb: 100290
```
So speed increase by about 12, avragage duration is reduced by about 10.

24 Jan - start working on v2
============================


Testing for regressions: Eze asked to test for regressions.
Here is the test regressions branch:

