#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/dma-mapping.h>
#include <linux/dma-direct.h>

#define MY_DEV_NAME "my-device"

#include "major-minor.h"



struct my_driver {
	struct platform_device* pdev;
	 char *buf;
};

/* =================== vm operations ==================== */
static void vm_open(struct vm_area_struct *vma)
{
	pr_info("%s\n", __func__);
}

static void vm_close(struct vm_area_struct *vma)
{
	pr_info("vm_close\n");
}

static vm_fault_t vm_fault(struct vm_fault *vmf)
{

	pr_info("vm_fault\n");
	vmf->page = alloc_page(GFP_KERNEL);
	return 0;
}


static struct vm_operations_struct vm_ops = {
	.close = vm_close,
	.fault = vm_fault,
	.open = vm_open,
};

/* ======================================== */

static int switch_mmap(struct file *file, struct vm_area_struct *vma)
{
	pr_err("%s called\n", __func__);
	pr_err("mapped at 0x%08lx\n", vma->vm_start);
	vma->vm_ops = &vm_ops;
	//vma->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP;
	//vm_open(vma);
	return 0;
}

static int mycdrv_open(struct inode *inode, struct file *file)
{
	pr_err("OPEN device\n");
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	return 0;
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.mmap = switch_mmap,
	.open = mycdrv_open,
	.release = mycdrv_release,
};

static unsigned int count = 1;
static struct cdev my_cdev;

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	dev_info(&pdev->dev,
		 "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static void my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static struct platform_device my_device = {
	.name = MY_DEV_NAME,
};


static int __init platform_driver_init(void)
{
	int ret;
	ret = platform_device_register(&my_device);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
	platform_device_unregister(&my_device);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
