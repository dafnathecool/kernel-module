/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <assert.h>
#include <fcntl.h> /* creat, O_CREAT */
#include <poll.h> /* poll */
#include <stdio.h> /* printf, puts, snprintf */
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS */
#include <unistd.h> /* read */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include <sys/mman.h>
#include "major-minor.h"


static const char *mydevpath = "/dev/mydev";
static const char *kfifo_path = "/dev/mykfifo";

int create_path(const char *path, int minor)
{
	int rc;

	dev_t id = makedev(my_major, minor);
	rc = mknod(path, S_IFCHR | S_IRWXU | S_IRWXG | S_IRWXO , id);
	if (rc && errno != EEXIST) {
		printf("mknod failed %d %d\n", rc, EEXIST);
		return rc;
	}
	return 0;
}

int main(int argc, char **argv)
{
	int fd, rc;
	//volatile int *ptr;
	char *ptr;
	char a;

	rc = create_path(mydevpath, my_minor);
	if (rc)
		exit(EXIT_FAILURE);

	/* if we want our 'mmap' to be called, we should use O_RDWR */
	//fd = open(mydevpath, O_RDONLY | O_NONBLOCK);
	fd = open(mydevpath, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	printf("about to mmap\n");
	/* if we want our 'mmap' to be called, we should avoid the MAP_ANON flag */
	//ptr = mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, fd, 0);
	ptr = mmap(NULL, 2 * 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	printf("about to write to ptr[0]\n");
	ptr[0] = 1;
	printf("about to write to ptr[4095]\n");
	ptr[4095] = 1;
	printf("about to read from ptr[4096]\n");

	a = ptr[4096];
}
