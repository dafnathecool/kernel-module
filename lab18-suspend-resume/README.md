SUSPEND RESUME
==============
There are several suspends in Linux:
suspend to ram: ram is still on, all other dies
suspend to disk: all goes to disk

The supported suspend operations are in /sys/power/state:
```
cat /sys/power/state
freeze mem disk
```
This 'state' sysfs file is created in kernel/power/main.c

Suspend to RAM
==============
Setting devices for wakeup from suspend
---------------------------------------
//TODO - is seems that waking up from keyboard did not work
Second let's set device (such as keyboard/mouse etc.) to wake up from suspend:
```
//see which devices have wakeup functionality and its status:
grep . /sys/bus/usb/devices/*/power/wakeup

//change all wakups to enable:
for f in `grep . /sys/bus/usb/devices/*/power/wakeup | grep -o "^.*wakeup"`; do echo $f ;  echo enabled > $f; done
```

on rock-pi-4 , it seems that something worng with the usb , wakeup with usb keyboard does
not seem to work.

I set the kernel param: `no_console_suspend`
and then entered suspned with:
```
echo mem > /sys/power/state
```
With that param log the debug was much more (verbose)[suspend-to-ram-demsg.log]

Suspend to RAM Flow:
--------------------
First compile with extensive pm-debugs config:
CONFIG_PM_SLEEP_DEBUG
CONFIG_PM_DEBUG
CONFIG_PM_ADVANCED_DEBUG

IN ADDITION, you should add the param (without value) 'no_console_suspend`, `pm_debug_messages` in the kernel params
and do `echo 1 > /sys/power/pm_debug_messages`


```
in `state_store` we see that for case `PM_SUSPEND_MEM`, the function is `pm_suspend(state)`.
 a bit further it is easy to see that the flow is:
 ```
state_store -> pm_suspend -> enter_state ->
				suspend_devices_and_enter(state) ->
					platform_suspend_begin ->
						suspend_ops->begin(state); //no begin cb for psci
					suspend_console();
					suspend_test_start(); // only set one val
					error = dpm_suspend_start(PMSG_SUSPEND);
					suspend_test_finish("suspend devices");

					//DAFNA - there is a way out!
					//let's check later what happen when setting
					//pm_test_level = TEST_DEVICE 
					if (suspend_test(TEST_DEVICES))
						goto Recover_platform;

					do {
						error = suspend_enter(state, &wakeup);
					} while (!error && !wakeup && platform_suspend_again(state));
```
Note the comment about the param `pm_test_level`, it seems to be used for testing -
suspending part of the system or something and the resueming.
`pm_test_level` can be set/get through `/sys/power/pm_test`
Also note that 'do-while' loop, let's see later what it does.
Let's check what is 'suspend_ops->begin for the `mem` suspend:
By adding `dump_stack` to func `suspend_set_ops` we can see it is called (for arm devices)
from `psci_probe`:
```
suspend_set_ops(&psci_suspend_ops);
```
So `psci_suspend_ops` is our ops struct, which has only the `valid` and `enter` cb
back to the dmesg log, note the message:
```
PM: start suspend of devices complete after 498.829 msecs
```
It coressponds to the print in `drivers/base/power/main.c`:
```
pm_pr_dbg("%s%s%s of devices %s after %ld.%03ld msecs\n"
```

Let's look at the function `dpm_suspend_start`:

```
dpm_suspend_start ->
	dpm_prepare(state);
	dpm_suspend(state);
```
Both dpm_prepare and dpm_suspend have a while loop they loop all devices and
execute the 'prepare' and 'suspend' cb respectivally.















//TODO - have a look at power tools/power/pm-graph/sleepgraph.py
